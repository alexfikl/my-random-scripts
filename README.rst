My Random Scripts
=================

This repository contains a random assortment of (mostly out of date) scripts.
They're mostly in Python too!

For a small rundown, we have

* ``benchmarks``: various small benchmarks performed over the years, which may
  no longer be accurate or representative.
* ``bin``: a bunch of useful scripts that go into `~/.local/bin` and have a good
  chance of being up to date.
* ``functionality``: various scripts testing out functionality of python stdlib
  or other libraries, as needed.
* ``scripts``: a bunch of scripts that have not yet made it into ``bin``, and
  perhaps never will, but were still useful at their time.

Install
=======

The scripts from ``bin`` are useful and can be installed to ``LOCAL_PREFIX`` using

.. code:: sh

    export LOCAL_PREFIX=~/.local
    make install-bin

This also installs zsh completion and other useful things.
