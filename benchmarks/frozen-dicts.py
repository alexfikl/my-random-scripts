# SPDX-FileCopyrightText: 2021 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from __future__ import annotations

import frozendict
import immutables
import libench  # noqa: F401
import pyrsistent


def run_dict(d, n=1_000_000, key="5"):
    for _ in range(n):
        d.get(key)
        d.get(key)
        d.get(key)
        d.get(key)
        d.get(key)

        d.get(key)
        d.get(key)
        d.get(key)
        d.get(key)
        d.get(key)


def run_builtin_dict(n):
    d = {}
    for i in range(n):
        d[str(i)] = i
    assert len(d) == n

    run_dict(d)


def run_immutables(n):
    d = immutables.Map()
    for i in range(n):
        d = d.set(str(i), i)
    assert len(d) == n

    run_dict(d)


def run_frozendict(n):
    d = frozendict.frozendict()
    for i in range(n):
        d = d.set(str(i), i)
    assert len(d) == n

    run_dict(d)


def run_pyrsistent(n):
    d = pyrsistent.pmap()
    for i in range(n):
        d = d.set(str(i), i)
    assert len(d) == n

    run_dict(d)


if __name__ == "__main__":
    import perfplot

    perfplot.save(
        "benchmark-frozen-dicts",
        transparent=False,
        setup=lambda n: n,
        kernels=[
            run_builtin_dict,
            run_immutables,
            run_frozendict,
            # NOTE: pyrsistent is way slower
            # run_pyrsistent,
        ],
        labels=["dict", "immutables", "frozendict", "pyrsistent"],
        n_range=[5, 10, 20, 30, 100, 200, 300, 400, 500, 1000, 1500, 2000],
        # relative_to=0,
        equality_check=None,
    )
