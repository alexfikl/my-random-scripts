# SPDX-FileCopyrightText: 2021 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""Benchmark pymbolic's flatten_sum and flatten_product.

Results

- 2021-11-16:
"""

from __future__ import annotations

import numpy as np
from pymbolic.primitives import Sum, Variable, is_zero


def flattened_sum_orig(components):
    # flatten any potential sub-sums
    queue = list(components)
    done = []

    while queue:
        item = queue.pop(0)

        if is_zero(item):
            continue

        if isinstance(item, Sum):
            queue += item.children
        else:
            done.append(item)

    if len(done) == 0:
        return 0
    elif len(done) == 1:
        return done[0]
    else:
        return Sum(tuple(done))


def flattened_sum_new(components):
    from collections import deque

    queue = deque(components)
    done = []

    while queue:
        item = queue.popleft()

        if isinstance(item, Sum):
            queue.extend(item.children)
        else:
            if is_zero(item):
                continue

            done.append(item)

    if len(done) > 1:
        return Sum(tuple(done))

    if len(done) == 0:
        return 0
    else:
        return done[0]


def generate_nested_sums(n, ndepth=3):
    rng = np.random.default_rng()
    i = 0

    def sums(level):
        nonlocal i

        children = []
        for _ in range(n):
            coin = rng.integers(2 if level == 0 else 3)

            if coin == 0:
                item = 0
            elif coin == 1:
                item = Variable(f"x{i}")
                i += 1
            else:
                item = sums(level - 1)

            children.append(item)

        return Sum(tuple(children))

    return [sums(ndepth - 1) for _ in range(n)]


if __name__ == "__main__":
    import matplotlib.pyplot as mp
    import perfplot

    mp.rc("figure", figsize=(10, 10), dpi=300)

    perfplot.save(
        "pymbolic-flatten-sum-benchmark",
        transparent=False,
        setup=generate_nested_sums,
        kernels=[
            flattened_sum_orig,
            flattened_sum_new,
        ],
        time_unit="ms",
        labels=["orig", "new"],
        n_range=range(2, 32, 4),
        equality_check=None,
        # relative_to=1,
    )
