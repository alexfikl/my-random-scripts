# SPDX-FileCopyrightText: 2021 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from __future__ import annotations


def _initialize_matplotlib_defaults() -> None:
    import matplotlib
    import matplotlib.pyplot as mp

    # FIXME: this does not seem to be a documented way to check for tex support,
    # but it works for our current usecase. it also does not catch the actual
    # issue on porter, which is the missing `cm-super` package
    usetex = matplotlib.checkdep_usetex(s=True)

    mp.rc("figure", figsize=(15, 10), dpi=300)
    mp.rc("figure.constrained_layout", use=True)
    mp.rc("text", usetex=usetex)
    mp.rc("legend", fontsize=24)
    mp.rc("lines", linewidth=2.5, markersize=10)
    mp.rc("axes", labelsize=32, titlesize=32)
    mp.rc("xtick", labelsize=24)
    mp.rc("ytick", labelsize=24)

    try:
        import matplotx

        mp.style.use(matplotx.styles.dufte)
    except ImportError:
        pass


_initialize_matplotlib_defaults()


# {{{ pyopencl


def get_cl_context(*, default: bool = True):
    import pyopencl as cl

    if default:
        return cl.create_some_context()

    platforms = cl.get_platforms()
    devices = platforms[0].get_devices()

    context = cl.Context(
        devices=[devices[0]],
        properties=[(cl.context_properties.PLATFORM, platforms[0])],
    )

    return context


# }}}
