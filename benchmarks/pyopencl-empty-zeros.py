# SPDX-FileCopyrightText: 2021 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from __future__ import annotations

from functools import partial

import libench  # noqa: F401
import numpy as np
import pyopencl as cl
import pyopencl.array as cla


def run_empty(queue, allocator, n):
    cla.empty(queue, n, dtype=np.float64, allocator=allocator)


def run_zeros(queue, allocator, n):
    cla.empty(queue, n, dtype=np.float64, allocator=allocator)


if __name__ == "__main__":
    import perfplot

    ctx = cl.create_some_context()
    with cl.CommandQueue(ctx) as queue:
        from pyopencl.tools import ImmediateAllocator

        allocator = ImmediateAllocator(queue)

        perfplot.save(
            "pyopencl-empty-zeros",
            transparent=False,
            setup=lambda n: n,
            kernels=[
                partial(run_empty, queue, allocator),
                partial(run_zeros, queue, allocator),
            ],
            labels=["empty", "zeros"],
            n_range=[2**k for k in range(24)],
            equality_check=None,
            time_unit="ms",
        )
