# SPDX-FileCopyrightText: 2021 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""Benchmark if constructing a tuple from a list or a generator is faster.

The value being iterated over is assumed to already be constructed, so the
only speed difference is in actually putting everything together.

Results

- 2021-03-03: the list version is ever so slightly faster, but both are order
  of microseconds
"""

from __future__ import annotations

import numpy as np


def like_me(x):
    return tuple(x)


def run_with_generator(y):
    return like_me(yi + 1 for yi in y)


def run_with_list(y):
    return like_me([yi + 1 for yi in y])


if __name__ == "__main__":
    import perfplot

    perfplot.save(
        "tuple-generator-v1-benchmark",
        transparent=False,
        setup=lambda n: [np.random.rand(n) for _ in range(3)],
        kernels=[
            run_with_generator,
            run_with_list,
        ],
        time_unit="ms",
        labels=["gen", "list"],
        n_range=[2**k for k in range(16)],
        # relative_to=1,
    )

    perfplot.save(
        "tuple-generator-v2-benchmark",
        transparent=False,
        setup=lambda n: [np.random.rand(5) for _ in range(n)],
        kernels=[
            run_with_generator,
            run_with_list,
        ],
        time_unit="ms",
        labels=["gen", "list"],
        n_range=[2**k for k in range(12)],
        # relative_to=1,
    )
