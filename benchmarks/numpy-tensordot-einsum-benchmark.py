# SPDX-FileCopyrightText: 2021 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
Benchmark
"""

from __future__ import annotations

import numpy as np


def run_with_tensordot(data):
    mat, vec = data
    return np.tensordot(mat, vec, axes=1)


def run_with_einsum(data):
    mat, vec = data
    return np.einsum("ij,jkl->ikl", mat, vec, optimize=True)


if __name__ == "__main__":
    import perfplot

    perfplot.save(
        "numpy-tensordot-einsum-benchmark",
        transparent=False,
        setup=lambda n: (np.random.rand(10, 17), np.random.rand(17, n, 42)),
        kernels=[
            run_with_tensordot,
            run_with_einsum,
        ],
        labels=["tensordot", "einsum"],
        n_range=np.arange(100, 10000, 500),
    )
