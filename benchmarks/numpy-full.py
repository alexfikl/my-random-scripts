# SPDX-FileCopyrightText: 2021 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""Benchmark various :mod:`numpy` methods to allocate an array filled with a
scalar value.

Results:

- 2021-03-01: turns out empty + fill does the best job, but they're all very
    fast as it mostly depends on the allocator and a `memset`.
"""

from __future__ import annotations

import numpy as np
import perfplot


def np_empty_set(n):
    x = np.empty((3, n), dtype=np.int64)
    x[:] = -1
    return x


def np_empty_fill(n):
    x = np.empty((3, n), dtype=np.int64)
    x.fill(-1)
    return x


def np_full(n):
    x = np.full((3, n), -1, dtype=np.int64)
    return x


if __name__ == "__main__":
    perfplot.save(
        "numpy-full-benchmark",
        transparent=False,
        setup=lambda n: n,
        kernels=[
            np_empty_set,
            np_empty_fill,
            np_full,
        ],
        labels=["set", "fill", "full"],
        n_range=[2**k for k in range(18)],
        relative_to=2,
    )
