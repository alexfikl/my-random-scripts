# SPDX-FileCopyrightText: 2021 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from __future__ import annotations

from functools import partial

import libench
import numpy as np
import perfplot
import pyopencl as cl


def _get_cl_program(context):
    return cl.Program(
        context,
        """
    __kernel void sum_matrices(
        __global const float *a_g, __global const float *b_g, __global float *res_g)
    {
      int gid = get_global_id(0);
      res_g[gid] = a_g[gid] + b_g[gid];
    }
    """,
    ).build()


def _get_cl_buffer(queue, n, dtype=np.float32):
    a_np = np.random.rand(n).astype(dtype)
    b_np = np.random.rand(n).astype(dtype)

    mf = cl.mem_flags
    a = cl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=a_np)
    b = cl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=b_np)
    r = cl.Buffer(ctx, mf.WRITE_ONLY, a_np.nbytes)

    return a, b, r


def _run_pyopencl(queue, program, bufs, *, timing: bool = False):
    a_g, b_g, res_g = bufs
    knl = program.sum_matrices

    if timing:
        import time

        t_start = time.time()

    index = 0
    t_exec = 0.0
    while index < 100:
        knl.set_args(a_g, b_g, res_g)
        event = cl.enqueue_nd_range_kernel(queue, knl, (a_g.size,), None)
        event.wait()

        t_exec += (event.profile.end - event.profile.start) * 1.0e-9
        index += 1

    if timing:
        t_end = time.time()

        print(f"execution time cost: {t_exec}")
        print(f"total time cost: {t_end - t_start}")


if __name__ == "__main__":
    ctx = libench.get_cl_context()
    prg = _get_cl_program(ctx)

    with cl.CommandQueue(ctx) as queue:
        perfplot.save(
            "pyopencl-enqueue-matrix-sum",
            transparent=False,
            setup=partial(_get_cl_buffer, queue, dtype=np.float32),
            kernels=[
                partial(_run_pyopencl, queue, prg),
            ],
            labels=["pyopencl"],
            n_range=[2**k for k in range(16)],
            equality_check=None,
            time_unit="ms",
        )
