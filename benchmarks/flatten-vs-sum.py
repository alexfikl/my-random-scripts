# SPDX-FileCopyrightText: 2021 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""Benchmark single level flattening.

Results

- 2021-10-13: the `sum` variant seems to be consistently slower by an order
  of magnitude or so.
"""

from __future__ import annotations

import numpy as np
from pytools import flatten


def flatten_with_generator(y):
    return list(flatten([x] * n for x, n in y))


def flatten_with_sum(y):
    return sum(([x] * n for x, n in y), [])  # noqa: RUF017


def flatten_with_append(y):
    r = []
    for x, n in y:
        r.extend([x] * n)

    return r


def flatten_with_comprehension(y):
    return [x for x, n in y for _ in range(n)]


if __name__ == "__main__":
    import perfplot

    rng = np.random.default_rng()

    perfplot.save(
        "flatten-sum-benchmark",
        transparent=False,
        setup=lambda n: [(0, rng.integers(7)) for _ in range(n)],
        kernels=[
            flatten_with_generator,
            flatten_with_sum,
            flatten_with_append,
            flatten_with_comprehension,
        ],
        time_unit="ms",
        labels=["gen", "sum", "append", "comprehension"],
        n_range=range(2, 128),
        # relative_to=1,
    )
