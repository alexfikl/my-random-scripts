# SPDX-FileCopyrightText: 2021 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from __future__ import annotations

import pathlib
from dataclasses import dataclass

import numpy as np

# {{{


@dataclass(frozen=True)
class TimeResult:
    walltime: float
    mean: float
    std: float

    def __str__(self):
        return f"wall {self.walltime:.3e}s mean {self.mean:.3e}s ± {self.std:.3e}"


def timeit(stmt, *, setup="pass", repeat: int = 8, number: int = 1) -> TimeResult:
    import timeit as _timeit

    r = _timeit.repeat(stmt=stmt, setup=setup, repeat=repeat + 1, number=number)
    r = np.array(r)

    return TimeResult(walltime=np.min(r), mean=np.mean(r), std=np.std(r))


# }}}


def read_papis_library_yaml(dirname: pathlib.Path) -> None:
    import yaml

    for path in dirname.iterdir():
        if not path.is_dir():
            continue

        info_yaml = path / "info.yaml"
        if not info_yaml.exists():
            continue

        with open(info_yaml, encoding="utf-8") as f:
            out = yaml.load(f, Loader=yaml.SafeLoader)

        assert out["author"]


def read_papis_library_yaml_c(dirname: pathlib.Path) -> None:
    import yaml

    for path in dirname.iterdir():
        if not path.is_dir():
            continue

        info_yaml = path / "info.yaml"
        if not info_yaml.exists():
            continue

        with open(info_yaml, encoding="utf-8") as f:
            out = yaml.load(f, Loader=yaml.CSafeLoader)

        assert out["author"]


def read_papis_library_ruamel(dirname: pathlib.Path) -> None:
    from ruamel.yaml import YAML

    for path in dirname.iterdir():
        if not path.is_dir():
            continue

        info_yaml = path / "info.yaml"
        if not info_yaml.exists():
            continue

        with open(info_yaml, encoding="utf-8") as f:
            yaml = YAML(typ="safe", pure=False)
            out = yaml.load(f)

        assert out["author"]


def read_papis_library_rapidyaml(dirname: pathlib.Path) -> None:
    import ryml

    for path in dirname.iterdir():
        if not path.is_dir():
            continue

        info_yaml = path / "info.yaml"
        if not info_yaml.exists():
            continue

        with open(info_yaml, "rb") as f:
            out = ryml.parse(f.read())

        assert out["author"]


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("dirname", type=pathlib.Path)
    args = parser.parse_args()

    # {{{ run

    # print("[yaml]       Running...")
    # r = timeit(lambda: read_papis_library_yaml(args.dirname))
    # print("[yaml]       {}".format(r))

    print("[yaml_c]     Running...")
    r = timeit(lambda: read_papis_library_yaml_c(args.dirname))
    print(f"[yaml_c]     {r}")

    # print("[ruamel]     Running...")
    # r = timeit(lambda: read_papis_library_ruamel(args.dirname))
    # print("[ruamel] {}".format(r))

    print("[rapidyaml]  Running...")
    r = timeit(lambda: read_papis_library_rapidyaml(args.dirname))
    print(f"[rapidyaml]  {r}")

    # }}}
