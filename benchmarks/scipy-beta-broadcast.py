# SPDX-FileCopyrightText: 2021 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

r"""
Computes a double sum involving Gamma and Beta functions

.. math::

    \sum_k^v \sum_m^{r} \binom{v}{k} \binom{r}{m}
        a^{v - k} (1 - a - b)^{r - m} b^{k + m}
        \mathrm{B}(\alpha + k, \beta + m)

where :math:`r = 100 - v` and :math:`a, b < 1`.

Results:
- 2021-03-03: the gamma version is noticeably faster.
"""

from __future__ import annotations

import numpy as np
import scipy.special as ss


def run_with_beta(v, a=1.0 / 2.0, b=3.0 / 7.0, alpha=2.0 / 3.0, beta=17.0 / 6.0):
    r = 100 - v
    k = np.arange(v).astype(np.float64)
    m = np.arange(r).astype(np.float64)

    k_terms = ss.comb(v, k) * a ** (v - k)
    m_terms = ss.comb(r, m) * (1 - a - b) ** (r - m)

    # pylint: disable=no-member
    k = k.reshape(-1, 1)
    m = m.reshape(1, -1)
    km_terms = b ** (k + m) * ss.beta(alpha + k, beta + m)

    return np.einsum("i,j,ij", k_terms, m_terms, km_terms)


def run_with_gamma(v, a=1.0 / 2.0, b=3.0 / 7.0, alpha=2.0 / 3.0, beta=17.0 / 6.0):
    r"""Use the identity

    .. math::

        B(x, y) = \frac{\Gamma(x) \Gamma(y)}{\Gamma(x + y)}
    """

    r = 100 - v
    k = np.arange(v).astype(np.float64)
    m = np.arange(r).astype(np.float64)

    k_terms = ss.gamma(alpha + k) * ss.comb(v, k) * a ** (v - k)
    m_terms = ss.gamma(beta + m) * ss.comb(r, m) * (1 - a - b) ** (r - m)

    k = k.reshape(-1, 1)
    m = m.reshape(1, -1)
    km_terms = b ** (k + m) / ss.gamma(alpha + beta + k + m)

    return np.einsum("i,j,ij", k_terms, m_terms, km_terms)


if __name__ == "__main__":
    import perfplot

    perfplot.save(
        "scipy-beta-broadcast-benchmark",
        transparent=False,
        setup=lambda n: n,
        kernels=[
            run_with_gamma,
            run_with_beta,
        ],
        labels=["gamma", "beta"],
        n_range=np.arange(5, 100, 10),
    )
