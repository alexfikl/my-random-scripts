# SPDX-FileCopyrightText: 2021 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""Benchmark whether a :class:`deque` is any good for very small sizes
that get added to repeatedly.

Results:

- 2021-03-01: using a :class:`deque` is a bit faster if we're doing this more
    than 1000 times.
"""

from __future__ import annotations

from collections import deque
from dataclasses import dataclass, field

import perfplot


@dataclass
class DequeContainer:
    history: deque = field(default_factory=lambda: deque(maxlen=5))


@dataclass
class ListContainer:
    history: list = field(default_factory=list)


def add_to_deque(n):
    c = DequeContainer()

    for _ in range(n):
        c.history.appendleft(42)

    return 1


def add_to_list(n):
    c = ListContainer()

    for _ in range(n):
        c.history.insert(0, 42)
        if len(c.history) > 5:
            c.history.pop()

    return 1


if __name__ == "__main__":
    perfplot.save(
        "short-deque-benchmark",
        transparent=False,
        setup=lambda n: n,
        kernels=[
            add_to_deque,
            add_to_list,
        ],
        labels=["deque", "list"],
        n_range=[2**k for k in range(18)],
        # relative_to=1,
    )
