# SPDX-FileCopyrightText: 2021 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
Benchmark a simple AXPY with :mod:`pyclblast`.

NOTE: Run a couple of times, since everyone generates and caches code, so first
runs will likely be slow.

Results

- 2021-03-06: :mod:`pyopencl` seems to be doing a lot better with its elementwise
  kernel. The code in :mod:`pyclblast` may not be tuned for my machine though.

- 2021-09-16: :mod:`pyopencl` still seems to be doing a lot better, with
  :mod:`pyclblast` being about 60% slower.
"""

from __future__ import annotations

from functools import partial

import numpy as np
import perfplot
import pyopencl as cl
import pyopencl.array

try:
    import pyclblast
except ImportError as exc:
    raise RuntimeError("script requires 'pyclblast'") from exc


def axpy_pyclblast(queue, x):
    x, y = x

    # pylint: disable=c-extension-no-member
    for _ in range(5):
        pyclblast.axpy(queue, x.size, x, y, alpha=1.5)
    queue.finish()


def axpy_pyopencl(queue, x):
    x, y = x

    # pylint: disable=protected-access
    for _ in range(5):
        cl.array.Array._axpbyz(y, 1.5, x, 1, y)
    queue.finish()


def allocate(queue, n, dtype=None):
    x = cl.array.to_device(queue, np.linspace(0.0, 2.0 * np.pi, n, dtype=dtype))
    y = cl.array.to_device(queue, np.linspace(-1.0, 1.0, n, dtype=dtype))
    queue.finish()

    return x, y


if __name__ == "__main__":
    ctx = cl.create_some_context()
    queue = cl.CommandQueue(ctx)

    perfplot.save(
        "clblast-axpy-benchmark",
        transparent=False,
        setup=partial(allocate, queue, dtype=np.float64),
        kernels=[
            partial(axpy_pyclblast, queue),
            partial(axpy_pyopencl, queue),
            partial(axpy_pyclblast, queue),
        ],
        labels=["dummy", "pyopencl", "pyclblast"],
        n_range=[2**k for k in range(14)],
        relative_to=1,
        equality_check=None,
    )
