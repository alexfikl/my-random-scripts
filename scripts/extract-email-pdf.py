#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2023 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from __future__ import annotations

import logging
import pathlib
import re

import numpy as np
import rich.logging

logger = logging.getLogger(pathlib.Path(__file__).stem)
logger.setLevel(logging.ERROR)
logger.addHandler(rich.logging.RichHandler())


def extract_emails_from_pdf(
    filename: pathlib.Path,
    *,
    lang: str = "eng",
    exclude: tuple[str, ...] = "psihoreliconstruct@yahoo.com",
) -> int:
    if not filename.exists():
        logger.error("file does not exist: '%s'", filename)
        return 1

    import PyPDF2

    reader = PyPDF2.PdfReader(filename)

    if len(reader.pages) == 0:
        logger.error("file has no pages: '%s'", filename)
        return 1

    # {{{ read image from pdf

    page = reader.pages[0]
    if len(page.images) == 0:
        logger.error("no images found on page 1")
        return 1

    pdf_img = page.images[0]

    # }}}

    # {{{ preprocess using opencv

    import cv2

    # decode
    cv_img = cv2.imdecode(np.frombuffer(pdf_img.data, np.int8), cv2.IMREAD_COLOR)

    # clip the top half with the email
    n = cv_img.shape[0] // 4
    top_img = cv_img[:n, :]

    cv2.imwrite("result.png", top_img)

    # }}}

    # {{{ ocr

    import pytesseract

    text = pytesseract.image_to_string(top_img, config="--psm 6", lang=lang)

    re_email = re.compile(r"[\w.+-]+@[\w\s-]+[\w-]+\.[\w.-]+")

    emails = []
    for result in re_email.findall(text):
        match = result.replace("\n", "")
        if match not in exclude:
            emails.append(match)

    print(emails)

    # }}}

    # {{{ send email

    import email
    import email.encoders
    import email.mime
    import email.mime.multipart
    import email.mime.text
    import smtplib
    import ssl

    message = email.mime.multipart.MIMEMultipart()
    # message["From"] = "todo@email.com"
    # message["To"] = emails
    message["To"] = "todo@example.com"
    message["Subject"] = "Email Test"

    lines = [
        "Hi, hello,\n",
        "These are the emails I extracted from the attached pdf:",
        *emails,
        "\nYours truly",
    ]

    body = email.mime.text.MIMEText("\n".join(lines), "plain")
    message.attach(body)

    with open(filename, "rb") as attachment:
        part = email.mime.base.MIMEBase("application", "pdf")
        part.set_payload(attachment.read())

    email.encoders.encode_base64(part)
    part.add_header("Content-Disposition", f"attachment; filename={filename.name}")
    message.attach(part)

    text = message.as_string()

    import shutil
    import subprocess  # noqa: S404

    exe = shutil.which("gopass")
    if exe is None:
        logger.error("'gopass' executable cannot be found.")
        return 1

    r = subprocess.run(  # noqa: S603
        [exe, "show", "-o", "google.com/dev"],
        capture_output=True,
        check=True,
    )
    password = r.stdout.decode()

    context = ssl.create_default_context()
    with smtplib.SMTP_SSL("smtp.gmail.com", 465, context=context) as server:
        server.login(message["From"], password)
        server.sendmail(message["From"], message["To"], text)

    # }}}

    return 0


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "filename", type=pathlib.Path, help="a PDF file from which to extract"
    )
    parser.add_argument(
        "-q", "--quiet", action="store_true", help="only show error messages"
    )
    args = parser.parse_args()

    if not args.quiet:
        logger.setLevel(logging.INFO)

    raise SystemExit(extract_emails_from_pdf(args.filename))
