#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2023 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from __future__ import annotations

import logging
import pathlib
import shutil
import subprocess  # noqa: S404

import rich.logging

logger = logging.getLogger(pathlib.Path(__file__).stem)
logger.setLevel(logging.ERROR)
logger.addHandler(rich.logging.RichHandler())


# {{{ utils


def is_tex(entry: pathlib.Path) -> bool:
    return entry.is_file() and entry.suffix == ".tex"


def get_output_name(outdir: pathlib.Path, basename: str) -> pathlib.Path:
    index = 1
    count = ""

    while True:
        filename = outdir / f"{basename}{count}.pdf"
        if not filename.exists():
            break

        count = f"-{index}"
        index += 1

    return filename


# }}}


# {{{ compile


def compile_file(
    filename: pathlib.Path,
    builddir: pathlib.Path,
    outdir: pathlib.Path,
) -> None:
    basename = filename.parent.name
    output = get_output_name(outdir, basename)
    logger.info("Building: '%s'", filename)

    # compile latex file
    subprocess.check_output([  # noqa: S603,S607
        "latexmk",
        "-cd",
        "-pdf",
        str(filename),
        f"-outdir={builddir}",
        "-bibtex-cond",
        "-f",
        "-silent",
    ])

    # move result to output directory
    logger.info("Moving pdf to '%s'", output)

    pdf = filename.parent / builddir / f"{filename.stem}.pdf"
    shutil.move(pdf, output)


def compile_directory(
    directory: pathlib.Path,
    builddir: str,
    outdir: pathlib.Path,
    *,
    clean: bool = True,
    ignore: tuple[str, ...] | None = None,
) -> None:
    logger.info("Processing directory: '%s'", directory)

    for d in directory.iterdir():
        breakpoint()
        if d.name in ignore or d.name[0] == ".":
            continue

        if d.is_dir():
            compile_directory(d, builddir, outdir, clean=clean, ignore=ignore)
        elif is_tex(d):
            compile_file(d, builddir, outdir)
        else:
            # ignoring all other file types
            pass

    builddir = directory / builddir
    if clean and builddir.exists():
        shutil.rmtree(builddir)


# }}}


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "directories",
        nargs="*",
        type=pathlib.Path,
        help="directories to parse and build",
    )
    parser.add_argument(
        "--clean", action="store_false", help="remove build directory after compilation"
    )
    parser.add_argument("--build", default="latex.out", help="name of build directory")
    parser.add_argument(
        "--output", type=pathlib.Path, default="slides", help="output directory"
    )
    parser.add_argument(
        "--ignore",
        default="img,latex.out,xpacc-macros.tex",
        help="file names or directories to ignore",
    )
    parser.add_argument("-q", "--quiet", action="store_true")
    args = parser.parse_args()

    if not args.quiet:
        logger.setLevel(logging.INFO)

    ignores = (*args.ignore.split(","), str(args.build))
    output = args.output.resolve()
    if not output.exists():
        output.mkdir()

    directories = args.directories
    if not directories:
        directories = [d for d in pathlib.Path.cwd().iterdir() if d.is_dir()]
    directories = [d.resolve() for d in directories]

    for d in directories:
        compile_directory(d, args.build, output, ignore=ignores, clean=args.clean)
