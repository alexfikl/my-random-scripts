#!/bin/bash

# SPDX-FileCopyrightText: 2023 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

set -o errexit -o nounset

# See https://wiki.archlinux.org/title/System_maintenance

# {{{ systemd errors

echo -e '\x1b[1;97m===> \x1b[1;32mFailed systemd Units\x1b[0m'
systemctl --failed || true
echo -e ''

echo -e '\x1b[1;97m===> \x1b[1;32mLog File Errors (Priority 3)\x1b[0m'
journalctl -p 3 -b || true
echo -e ''

# }}}

# {{{ pacman errors

echo -e '\x1b[1;97m===> \x1b[1;32mOrphan Packages\x1b[0m'
yay -Qdt || true
echo -e ''

echo -e '\x1b[1;97m===> \x1b[1;32m.pacnew Files\x1b[0m'
pacdiff -o || true
echo -e ''

echo -e '\x1b[1;97m===> \x1b[1;32mClean Package Cache\x1b[0m'
yay -Scc || true
echo -e ''

# }}}
