#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2023 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from __future__ import annotations

import logging
import pathlib

import numpy as np
import rich.logging

logger = logging.getLogger(pathlib.Path(__file__).stem)
logger.setLevel(logging.ERROR)
logger.addHandler(rich.logging.RichHandler())


# {{{


def crop_min_area_rect(img: "np.ndarray", rect: "np.ndarray") -> "np.ndarray":
    import cv2

    angle = rect[2]
    nrows, ncols, _ = img.shape

    # rotate image
    M = cv2.getRotationMatrix2D((ncols // 2, nrows // 2), angle, 1)
    rot = cv2.warpAffine(img, M, (ncols, nrows))

    # rotate bounding box
    rect0 = (rect[0], rect[1], 0.0)
    box = cv2.boxPoints(rect0)
    pts = np.int0(cv2.transform(np.array([box]), M))
    pts[pts < 0] = 0

    # crop
    xmin, xmax = pts[0, 1, 1], pts[0, 0, 1]
    ymin, ymax = pts[0, 1, 0], pts[0, 2, 0]
    breakpoint()

    return rot[xmin:xmax, ymin:ymax]


# }}}


# {{{ extract


def extract_photos(infile: pathlib.Path, outfile: pathlib.Path | None = None) -> int:
    if not infile.exists():
        logger.error("file does not exist: %s", infile)
        return 1

    if outfile is None:
        outfile = pathlib.Path(f"{infile.stem}_out{infile.suffix}").resolve()

    import cv2

    img = cv2.imread(str(infile))
    gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    _, cnt_img = cv2.threshold(
        gray_img, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_TRIANGLE
    )
    # cnt_img = cv2.Canny(gray_img, 20, 250)

    cnts, _ = cv2.findContours(
        cnt_img, mode=cv2.RETR_EXTERNAL, method=cv2.CHAIN_APPROX_NONE
    )
    logger.info("found %d contours", len(cnts))

    areas = np.array([cv2.contourArea(cnt) for cnt in cnts])
    if areas.size > 10:  # noqa: SIM108
        min_area = np.min(areas[areas > 5]) + 10
    else:
        min_area = 0.0
    logger.info("areas: min %.12e max %.12e", min_area, np.max(areas))

    cnts = tuple([cnts[i] for i in range(areas.size) if areas[i] > min_area])
    logger.info("filtered %d contours", len(cnts))

    img2 = img.copy()
    cv2.drawContours(
        img2,
        cnts,
        contourIdx=-1,
        color=(0, 255, 0),
        thickness=2,
        lineType=cv2.LINE_AA,
    )
    for i, cnt in enumerate(cnts):
        rect = cv2.minAreaRect(cnt)
        pts = np.int0(cv2.boxPoints(rect))
        cv2.drawContours(img2, [pts], contourIdx=-1, color=(255, 0, 0), thickness=2)

        crop_img = crop_min_area_rect(img, rect)
        if crop_img.size == 0:
            continue

        cv2.imwrite(
            str(outfile.parent / f"{outfile.stem}_{i:09d}{outfile.suffix}"), crop_img
        )

    logger.info("writing to '%s'", outfile)
    cv2.imwrite(str(outfile), img2)
    cv2.imwrite(f"{infile.stem}_thresh{infile.suffix}", cnt_img)

    return 0


# }}}


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "infile",
        type=pathlib.Path,
        help="scan of photo groups to extract",
    )
    parser.add_argument(
        "--outfile", type=pathlib.Path, default=None, help="basename of output files"
    )
    parser.add_argument(
        "-q", "--quiet", action="store_true", help="only show error messages"
    )
    args = parser.parse_args()

    if not args.quiet:
        logger.setLevel(logging.INFO)

    raise SystemExit(extract_photos(args.infile, args.outfile))
