#!/usr/bin/env python

# SPDX-FileCopyrightText: 2023 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from __future__ import annotations

import logging
import os
import pathlib

import platformdirs
import rich.logging

logger = logging.getLogger(pathlib.Path(__file__).stem)
logger.setLevel(logging.ERROR)
logger.addHandler(rich.logging.RichHandler())

SCRIPT_CONFIG_DIR = "yorick.scripts.com"
CHAT_GPT_CONFIG_PATH = (
    pathlib.Path(platformdirs.user_config_dir(SCRIPT_CONFIG_DIR)) / "chatgpt.ini"
)

CHAT_GPT_ENGINES = frozenset([
    "ada",
    "ada-code-search-code",
    "ada-code-search-text",
    "ada-search-document",
    "ada-search-query",
    "ada-similarity",
    "babbage",
    "babbage-002",
    "babbage-code-search-code",
    "babbage-code-search-text",
    "babbage-search-document",
    "babbage-search-query",
    "babbage-similarity",
    "code-davinci-edit-001",
    "code-search-ada-code-001",
    "code-search-ada-text-001",
    "code-search-babbage-code-001",
    "code-search-babbage-text-001",
    "curie",
    "curie-instruct-beta",
    "curie-search-document",
    "curie-search-query",
    "curie-similarity",
    "davinci",
    "davinci-002",
    "davinci-instruct-beta",
    "davinci-search-document",
    "davinci-search-query",
    "davinci-similarity",
    "gpt-3.5-turbo",
    "gpt-3.5-turbo-0301",
    "gpt-3.5-turbo-0613",
    "gpt-3.5-turbo-16k",
    "gpt-3.5-turbo-16k-0613",
    "text-ada-001",
    "text-babbage-001",
    "text-curie-001",
    "text-davinci-001",
    "text-davinci-002",
    "text-davinci-003",
    "text-davinci-edit-001",
    "text-embedding-ada-002",
    "text-search-ada-doc-001",
    "text-search-ada-query-001",
    "text-search-babbage-doc-001",
    "text-search-babbage-query-001",
    "text-search-curie-doc-001",
    "text-search-curie-query-001",
    "text-search-davinci-doc-001",
    "text-search-davinci-query-001",
    "text-similarity-ada-001",
    "text-similarity-babbage-001",
    "text-similarity-curie-001",
    "text-similarity-davinci-001",
    "whisper-1",
])


def get_api_key(config: pathlib.Path) -> str | None:
    import configparser

    p = configparser.ConfigParser(interpolation=None)
    p.read(config)

    return p.get("keys", "chatgpt", fallback=None)


def main(
    prompt: str,
    *,
    api_key: str,
    engine: str = "text-davinci-002",
    max_tokens: int = 50,
    clip: bool = False,
) -> int:
    import openai

    openai.api_key = api_key

    try:
        response = openai.Completion.create(
            engine=engine,
            prompt=prompt,
            max_tokens=max_tokens,
            n=1,
            stop=None,
            temperature=0.7,
        )
    except Exception as exc:
        logger.error("Failed to query ChatGPT: '%s'", prompt, exc_info=exc)
        return 1

    text = response.choices[0].text.strip()
    print(f"Question:\n>> {prompt}")
    print(f"Answer:\n{text}")

    if clip:
        try:
            import pyperclip

            pyperclip.copy(text)
            logger.info("Copied to clipboard")
        except ImportError:
            logger.error("Copy to clipboard: 'pyperclip' not available")

    return 0


if __name__ == "__main__":
    import argparse

    import shtab

    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    shtab.add_argument_to(parser, ["--print-completion"])
    parser.add_argument("query", nargs="*")
    parser.add_argument(
        "-k",
        "--api-key",
        default=None,
        help="ChatGPT API key",
    )
    parser.add_argument(
        "--engine",
        metavar="ENGINE",
        choices=list(CHAT_GPT_ENGINES),
        default="text-davinci-002",
        help="engine to use for the prompt",
    )
    parser.add_argument(
        "--list-engines",
        action="store_true",
        help="list all known engines",
    )
    parser.add_argument(
        "-c", "--clip", action="store_true", help="copy AWB to system clipboard"
    )
    parser.add_argument(
        "-q", "--quiet", action="store_true", help="only show error messages"
    )
    args = parser.parse_args()

    if not args.quiet:
        logger.setLevel(logging.INFO)

    if args.list_engines:
        for engine in CHAT_GPT_ENGINES:
            print(engine)

    default_api_key = get_api_key(CHAT_GPT_CONFIG_PATH)
    api_key = args.api_key or os.environ.get("OPENAI_API_KEY") or default_api_key

    if api_key is None:
        logger.error("No API key has been provided.")
        raise SystemExit(1)

    raise SystemExit(main(" ".join(args.query), api_key=api_key, clip=args.clip))
