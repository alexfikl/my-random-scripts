#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2023 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

# This  script generates a LaTeX file from a list of PDF files given on the
# command line. It can be used as
#
# ```bash
# python generate-paper-registry *.pdf \
#   --title "Papers by John Snow" \
#   --output registry.tex \
#   --info-file info.json
# ```
#
# If given the `info.json` contains BibTeX data matched against the given PDFs.
# If a PDF is already inside the info file, we do not attempt to extract additional
# information from the file itself. On the other hand, if it is not, the extracted
# data is saved in the file so that it can be updated offline. This file is
# essentially there to allow

from __future__ import annotations

import json
import logging
import pathlib
import re
from dataclasses import asdict, dataclass, replace

import rich.logging

logger = logging.getLogger(pathlib.Path(__file__).stem)
logger.setLevel(logging.ERROR)
logger.addHandler(rich.logging.RichHandler(markup=True))


# {{{ utils


class ExtractionError(RuntimeError):
    pass


_re_clean_whitespace = re.compile(r"\s+")
_re_clean_fullstop = re.compile(r"[\.:;,]([^\d -])")
_re_clean_delimiter = re.compile(r"<.+?>")

_re_doi = re.compile(
    r"doi(.org)?"
    r"\s*(=|:|/|\()?\s*"
    r'("|\')?'
    r"(?P<doi>[^{fc}]+)"
    r'("|\'|\))?'.format(fc=r'"\s%$^\'<>@,;:#?&'),
    re.I,
)


def clean_string(value):
    value = _re_clean_whitespace.sub(" ", value).strip()
    value = _re_clean_fullstop.sub(lambda p: f". {p.group(1).upper()}", value)
    value = _re_clean_delimiter.sub("", value)

    return value


# }}}


# {{{ metadata


@dataclass(frozen=True)
class Author:
    firstname: str
    lastname: str

    def __str__(self):
        return f"{self.firstname[0]}. {self.lastname}"


@dataclass(frozen=True)
class Paper:
    filename: str

    authors: tuple[Author, ...] | None = None
    title: str | None = None
    journal: str | None = None
    year: str | None = None
    volume: str | None = None
    issue: str | None = None
    pages: str | None = None
    doi: str | None = None

    def __str__(self):
        if self.authors is None:
            return r"\textcolor{red}{\textbf{UNDEFINED}}"

        entries = []
        entries.append(", ".join(str(author) for author in self.authors))
        entries.append(f"\\textit{{{self.title}}}")
        entries.append(self.journal)

        if self.volume:
            entries.append(f"vol. {self.volume}")
        if self.issue:
            entries.append(f"no. {self.issue}")
        if self.pages:
            entries.append(f"pp. {self.pages}")
        if self.year:
            entries.append(self.year)

        return ", ".join(entries)


# }}}


# {{{ extract functions


def get_pdf_metadata(reader, *, verbose: bool = True) -> dict[str, str] | None:
    from PyPDF2.errors import PdfReadError

    try:
        meta = reader.metadata
    except PdfReadError as exc:
        if verbose:
            logger.error("%s: %s", type(exc).__name__, exc)
        return None

    if not meta:
        return None

    if not meta.author or not meta.title:
        return None
    else:
        return {"query_author": meta.author, "query_title": meta.title}


def get_xmp_metadata(reader, *, verbose: bool = True) -> dict[str, str] | None:
    from xml.parsers.expat import ExpatError

    from PyPDF2.errors import PdfReadError

    try:
        meta = reader.xmp_metadata
    except PdfReadError as exc:
        logger.error("    %s: %s", type(exc).__name__, exc)
        return None
    except ExpatError as exc:
        logger.error("    %s: %s", type(exc).__name__, exc)
        return None

    if not meta:
        return None

    try:
        if not meta.dc_publisher:
            return None
    except AttributeError:
        return None

    raise NotImplementedError


def get_doi_from_text(
    reader, *, maxpages: int = 3, verbose: bool = True
) -> dict[str, str] | None:
    doi = None
    for i in range(maxpages):
        text = reader.pages[i].extract_text()

        for result in _re_doi.finditer(text):
            doi = result.group("doi")
            break

        if doi:
            break

    if doi is None:
        return None
    else:
        return {"ids": [doi]}


def get_doi_from_pdfminer(
    filename: pathlib.Path,
    *,
    maxpages: int = 3,
    verbose: bool = True,
) -> dict[str, str] | None:
    try:
        from pdfminer.high_level import extract_text
    except ImportError:
        return None

    doi = None
    for i in range(maxpages):
        text = extract_text(filename, page_numbers=[i])

        for result in _re_doi.finditer(text):
            doi = result.group("doi")
            break

        if doi:
            break

    if doi is None:
        return None
    else:
        return {"ids": [doi]}


def gather_paper_information(
    filename: pathlib.Path,
    *,
    doi: str | None = None,
    verbose: bool = True,
) -> Paper:
    logger.info("'%s'", filename)
    result = Paper(filename.name)

    # {{{ get pdf info

    if not doi:
        import PyPDF2

        reader = PyPDF2.PdfReader(filename)

        metadata = get_pdf_metadata(reader, verbose=verbose)

        if metadata is None:
            metadata = get_xmp_metadata(  # pylint: disable=E1128
                reader, verbose=verbose
            )

        if metadata is None:
            metadata = get_doi_from_text(reader, verbose=verbose)

        if metadata is None:
            metadata = get_doi_from_pdfminer(filename, verbose=verbose)
    else:
        metadata = {"ids": [doi]}

    if metadata is None:
        logger.error("    Could not determine metadata for '%s'", filename)
        raise ExtractionError

    # }}}

    # {{{ search on crossref

    import habanero
    import requests

    cr = habanero.Crossref()
    try:
        works = cr.works(**metadata)
    except requests.exceptions.HTTPError as exc:
        logger.error("    Invalid crossref response for '%s': %s", metadata, exc)
        raise ExtractionError from exc

    work = None
    if "message" in works:
        if "items" in works["message"]:
            if works["message"]["items"]:
                work = works["message"]["items"][0]
        else:
            work = works["message"]

    if not work:
        logger.error("    Invalid crossref response for '%s': %s", metadata, works)
        raise ExtractionError

    # }}}

    def get_year(w):
        for key in ["issued", "published", "created"]:
            if key in w:
                break
        else:
            key = None

        if key is None:
            return ""
        else:
            return str(w[key]["date-parts"][0][0])

    result = replace(
        result,
        authors=[
            Author(firstname=author["given"], lastname=author["family"])
            for author in work["author"]
            if "given" in author
        ],
        title=clean_string(" ".join(work["title"])).capitalize(),
        journal=clean_string(work["container-title"][0]),
        year=get_year(work),
        volume=work.get("volume", ""),
        issue=work.get("issue", ""),
        pages=work.get("page", ""),
        doi=work.get("DOI", ""),
    )
    logger.info("    [bold white]Extracted[/]: %s", result)

    return result


# }}}


# {{{ generate_file

PDF_TEMPLATE = r"""\documentclass{scrartcl}
\usepackage[T1]{fontenc}

\usepackage{xparse}
\usepackage{scrlayer-scrpage}
\usepackage{amsmath}
\usepackage{hyperref}
\usepackage{cleveref}
\usepackage{lmodern}

\KOMAoptions{
    DIV=14,
    parskip=half*}

% custom headers and footers
\pagestyle{plain}
\clearscrheadfoot
\refoot[\pagemark]{\pagemark}
\rofoot[\pagemark]{\pagemark}

% less jarring links
\hypersetup{
    colorlinks=true,
    urlcolor=blue,
    citecolor=black,
    linkcolor=black
}

\title{((* title *))}
\date{}

\begin{document}

\maketitle

\begin{enumerate}
((( for paper in papers )))
    \item ((* paper *)). \href{run:((* paper.filename *))}{((* paper.filename *))}.
((( endfor )))
\end{enumerate}
\end{document}

% vim: filetype=tex nospell:
"""


def jinja_env():
    import jinja2

    env = jinja2.Environment(
        block_start_string="(((",
        block_end_string=")))",
        variable_start_string="((*",
        variable_end_string="*))",
        comment_start_string="((=",
        comment_end_string="=))",
        autoescape=True,
    )

    return env


def generate_file(
    outfile: pathlib.Path, title: str, papers: list[Paper], *, verbose: bool = True
) -> int:
    if not outfile.exists():
        logger.error("output file does not exist: '%s'", outfile)

    env = jinja_env()

    code = env.from_string(PDF_TEMPLATE).render(
        title=title,
        papers=papers,
    )

    outfile = outfile.with_suffix(".tex")
    with open(outfile, "w", encoding="utf-8") as outf:
        outf.write(code)

    logger.info("Writing report to '%s'", outfile)

    return 0


# }}}


# {{{ extract paper metadata


def extract_paper_metadata(
    pdfs: tuple[pathlib.Path, ...],
    output: pathlib.Path,
    *,
    title: str = "Registry",
    info_file: pathlib.Path | None = None,
) -> int:
    # {{{ load existing metadata

    if info_file and info_file.exists():
        logger.info("Loading metadata from '%s'", info_file)

        with open(info_file, encoding="utf-8") as infile:
            info = json.load(infile)
    else:
        info = {}

    # }}}

    # {{{ gather additional metadata for papers

    ret = 0
    papers = []
    success_count = 0
    for pdf in sorted(pdfs, key=lambda p: p.name):
        if not pdf.exists():
            ret |= 1
            logger.error("file does not exist: '%s' (skipping)", pdf)
            continue

        key = pdf.name
        paper = None
        if key in info:
            paper = Paper(**info[key])
            assert paper.filename == key

            if paper.authors:
                paper = replace(
                    paper, authors=tuple([Author(**author) for author in paper.authors])
                )
        else:
            paper = Paper(filename=pdf)

        # NOTE: only retrieve data if no authors are given
        if not paper.authors:
            try:
                paper = gather_paper_information(pdf, doi=paper.doi)
            except ExtractionError:
                ret |= 1
            else:
                info[key] = asdict(paper)

        success_count += int(paper.authors is not None)
        papers.append(paper)

    logger.info("Successful retrieval: %s / %s", success_count, len(papers))

    # }}}

    # {{{ save updated metadata

    if info_file:
        with open(info_file, "w", encoding="utf-8") as outfile:
            json.dump(info, outfile, indent=4, sort_keys=True)

        logger.info("Saved metadata to '%s'", info_file)

    # }}}

    # {{{ generate registry file

    ret |= generate_file(output, title, papers)

    # }}}

    return ret


# }}}

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "pdfs",
        nargs="*",
        type=pathlib.Path,
        help="a list of PDFs from which to extract metadata for the registry",
    )
    parser.add_argument("--title", default="Registry", help="title for the file")
    parser.add_argument(
        "--output",
        type=pathlib.Path,
        default="opis.tex",
        help="filename for the output registry file",
    )
    parser.add_argument(
        "--info-file",
        type=pathlib.Path,
        default=None,
        help="existing metadata for each PDF file (json)",
    )
    parser.add_argument(
        "-q", "--quiet", action="store_true", help="only show error messages"
    )
    args = parser.parse_args()

    if not args.quiet:
        logger.setLevel(logging.INFO)

    raise SystemExit(
        extract_paper_metadata(
            args.pdfs, args.output, title=args.title, info_file=args.info_file
        )
    )
