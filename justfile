PYTHON := 'python -X dev'

_default:
    @just --list

# {{{ formatting

alias fmt: format

[doc('Reformat all source code')]
format: isort black pyproject justfmt

[doc('Run ruff isort fixes over the source code')]
isort:
    ruff check --fix --select=I benchmarks bin functionality scripts
    ruff check --fix --select=RUF022 benchmarks bin functionality scripts
    @echo -e "\e[1;32mruff isort clean!\e[0m"

[doc('Run ruff format over the source code')]
black:
    ruff format benchmarks bin functionality scripts
    @echo -e "\e[1;32mruff format clean!\e[0m"

[doc('Run pyproject-fmt over the configuration')]
pyproject:
    {{ PYTHON }} -m pyproject_fmt \
        --indent 4 --max-supported-python '3.13' \
        pyproject.toml
    @echo -e "\e[1;32mpyproject clean!\e[0m"

[doc('Run just --fmt over the justfile')]
justfmt:
    just --unstable --fmt
    @echo -e "\e[1;32mjust --fmt clean!\e[0m"

# }}}
# {{{ linting

[doc('Run all linting checks over the source code')]
lint: typos reuse ruff mypy

[doc('Run typos over the source code and documentation')]
typos:
    typos --sort
    @echo -e "\e[1;32mtypos clean!\e[0m"

[doc('Check REUSE license compliance')]
reuse:
    {{ PYTHON }} -m reuse lint
    @echo -e "\e[1;32mREUSE compliant!\e[0m"

[doc('Run ruff checks over the source code')]
ruff:
    ruff check benchmarks bin functionality scripts
    @echo -e "\e[1;32mruff clean!\e[0m"

[doc('Run mypy checks over the source code')]
mypy:
    {{ PYTHON }} -m mypy bin
    @echo -e "\e[1;32mmypy clean!\e[0m"

# }}}

[doc("Remove generated files")]
clean:
    find . -type f -name '*.png' -delete

[doc("Install 'bin' scripts and completions")]
install:
    #!/usr/bin/env bash
    set -euo pipefail

    mkdir -p "${LOCAL_PREFIX}/bin"
    mkdir -p "${LOCAL_PREFIX}/usr/share/zsh/site-functions"

    for file in $(find bin -type f -name '*.py'); do
        name=$(basename "${file}");
        echo -e "Installing '\e[1;35m${name}\e[0m'...";
        install -m755 "${file}" -t "${LOCAL_PREFIX}/bin";
        python "${file}" --print-completion zsh \
            >| "${LOCAL_PREFIX}/usr/share/zsh/site-functions/_${name}";
    done

    for file in $(find bin -type f -name '*.sh'); do
        name=$(basename "$file"); \
        echo -e "Installing '\e[1;35m${name}\e[0m'..."; \
        install -m755 "${file}" -t "${LOCAL_PREFIX}/bin"; \
    done
    echo -e "\e[1;32mscripts installed!\e[0m"
