#! /usr/bin/env python3

# SPDX-FileCopyrightText: 2023 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT


from __future__ import annotations

import logging
import os
import pathlib
import sys
from collections.abc import Iterator
from contextlib import contextmanager
from dataclasses import dataclass
from typing import Any

import platformdirs
import pygit2
import rich.logging
from tqdm import tqdm

# {{{ script

logger = logging.getLogger(pathlib.Path(__file__).stem)
logger.setLevel(logging.ERROR)
logger.addHandler(rich.logging.RichHandler(markup=True))

SCRIPT_PATH = pathlib.Path(__file__)
SCRIPT_CONFIG_DIR = platformdirs.user_config_path("yorick.scripts.com")
SCRIPT_CONFIG_FILE = SCRIPT_CONFIG_DIR / "projects.toml"
SCRIPT_LONG_HELP = f"""\
A script to handle downloading and setting up repositories.

The configuration file can be found at

    {SCRIPT_CONFIG_FILE}

and has the following format

    [remotes]
    <NAME_1> = <git@my.remote.com>
    <NAME_2> = ...

    [<PROJECT_ID>]
    revision = <DEFAULT_BRANCH_NAME>
    remote = <DEFAULT_NAME_X>
    user = <DEFAULT_REPOSITORY_USER>
    upstream = <DEFAULT_UPSTREAM_USER>

    [<PROJECT_ID>.repositories]
    <REPO_NAME_1> = {{ revision = "<REVISION>"}}
    <REPO_NAME_2> = {{ user = "<USER>",
                       remote = "<REMOTE>",
                       revision = "<REVISION>",
                       upstream = "<UPSTREAM>"}}

In the repository definitions, all of the values are optional and take defaults
from the main project section. The remote URL is constructed as
`remotes[<remote>]:<user>/<repo-name>.git`.

Example:

    > {SCRIPT_PATH.name} --project <PROJECT_ID> --list
    > {SCRIPT_PATH.name} -p <PROJECT_ID> .
"""

# }}}


# {{{ git


class RemoteCallbacks(pygit2.RemoteCallbacks):  # type: ignore[misc,name-defined]
    def __init__(self, *, keyfile: str | None = None, quiet: bool = False) -> None:
        super().__init__()
        if keyfile is not None:
            keypath = pathlib.Path.home() / ".ssh" / keyfile
            keypair = pygit2.Keypair("git", f"{keypath}.pub", keypath, "")  # type: ignore[no-untyped-call]
        else:
            keypair = pygit2.KeypairFromAgent("git")  # type: ignore[no-untyped-call]

        self.quiet = quiet
        self.keypair = keypair
        self.tqdm: tqdm[Any] | None = None
        self.previous_objects = 0

    def credentials(
        self, url: str, username_from_url: str, allowed_types: int
    ) -> pygit2.Keypair | None:
        if allowed_types & pygit2.enums.CredentialType.SSH_KEY:
            return self.keypair

        return None

    def transfer_progress(self, stats: Any) -> None:
        if self.quiet:
            return

        if self.tqdm is None:
            self.tqdm = tqdm(total=stats.total_objects, ascii=True)

        assert self.tqdm is not None
        self.tqdm.update(stats.received_objects - self.previous_objects)
        self.previous_objects = stats.received_objects

    def __del__(self) -> None:
        if self.tqdm is not None:
            self.tqdm.close()


@contextmanager
def stashed(repo: pygit2.Repository) -> Iterator[None]:
    if len(list(repo.diff().deltas)) > 0:  # type: ignore[attr-defined]
        repo_name = pathlib.Path(repo.path).parent.name
        logger.info("    Stashing changes in '%s'", repo_name)

        repo.stash(repo.default_signature, "[pygit2] stashed for pull")  # type: ignore[attr-defined]
        try:
            yield None
        finally:
            repo.stash_pop()  # type: ignore[attr-defined]
    else:
        try:
            yield None
        finally:
            pass


@contextmanager
def branched(repo: pygit2.Repository, name: str) -> Iterator[None]:
    if repo.head.shorthand == name:
        try:
            yield None
        finally:
            pass
    else:
        repo_name = pathlib.Path(repo.path).parent.name
        logger.info(
            "    Checking out '%s' in '%s' (previously on '%s')",
            name,
            repo_name,
            repo.head.shorthand,
        )
        head = repo.head

        branch = repo.lookup_branch(name)
        ref = repo.lookup_reference(branch.name)
        repo.checkout(ref)  # type: ignore[attr-defined]

        try:
            yield None
        finally:
            repo.checkout(head)  # type: ignore[attr-defined]


def update_submodules(repo: pygit2.Repository) -> None:
    repo.submodules.update(init=True)  # type: ignore[attr-defined]
    submodules = repo.listall_submodules()

    path = pathlib.Path(repo.workdir)
    for submodule in submodules:
        logger.info("    Updating submodule '%s'", submodule)
        subrepo = pygit2.Repository(path / submodule)
        update_submodules(subrepo)


# }}}


# {{{ get project descriptions


@dataclass(frozen=True)
class Repository:
    name: str
    """The name of the remote repository."""
    revision: str
    """Name of the branch that should be checked out."""
    remote: str
    """Name of the remote URL used for the repository."""
    upstream: str
    """Name of the upstream URL used for the repository."""


@dataclass(frozen=True)
class Project:
    name: str
    """The name of a project."""
    keyfile: str | None
    """SSH key file used to clone the repositories in the project."""
    repos: dict[str, Repository]
    """A mapping for all the repositories in the project."""


def get_from_config(path: pathlib.Path) -> dict[str, Project]:
    if not path.exists():
        logger.error("Configuration file does not exist: '%s'", path)
        return {}

    import tomllib

    with open(path, "rb") as f:
        settings = tomllib.load(f)

    remotes = settings.pop("remotes", {})
    if not remotes:
        logger.error("Configuration does not define any remotes: '%s'", path)
        return {}

    projects = {}
    for pid, project in settings.items():
        defaults = {
            "revision": project.get("revision", "main"),
            "remote": project.get("remote", "github"),
            "user": project.get("user"),
            "upstream": project.get("upstream"),
        }

        repos = {}
        for rid, repo in project["repositories"].items():
            r = {**defaults, **repo}

            user = r["user"]
            if not user:
                logger.error(
                    "Repository '%s' of project '%s' does not define a user.", rid, pid
                )
                continue

            remote = r["remote"]
            url = remotes[remote]
            if url.startswith("git@"):
                remote_fmt = "{url}:{user}/{rid}.git"  # noqa: RUF027
            elif url.startswith("https:") or url.startswith("http:"):
                remote_fmt = "{url}/{user}/{rid}"  # noqa: RUF027
            else:
                logger.error("Remote '%s' has unsupported format: '%s'.", remote, url)
                continue

            upstream = remote_fmt.format(url=url, user=r["upstream"], rid=rid)
            remote = remote_fmt.format(url=url, user=user, rid=rid)

            repos[rid] = Repository(
                name=rid,
                remote=remote,
                revision=r["revision"],
                upstream=upstream,
            )

        if not repos:
            logger.error("No valid repositories configured in '%s'.", pid)
            continue

        projects[pid] = Project(
            name=pid,
            keyfile=project.get("keyfile"),
            repos=repos,
        )

    return projects


# }}}


# {{{ download projects


@dataclass(frozen=True)
class Status:
    returncode: int
    changed: bool


def pull_repository(
    path: pathlib.Path,
    repo: Repository,
    *,
    callbacks: RemoteCallbacks,
) -> Status:
    git_repo = pygit2.Repository(path / repo.name)

    if repo.revision not in git_repo.branches.local:  # type: ignore[attr-defined]
        logger.error("    Branch '%s' does not exist in repository", repo.revision)
        return Status(returncode=1, changed=False)

    changed = False
    with stashed(git_repo):  # noqa: SIM117
        with branched(git_repo, name=repo.revision):
            remotes = {remote.name: remote for remote in git_repo.remotes}  # type: ignore[attr-defined]

            if "upstream" not in remotes:
                logger.error("    Could not find upstream remote to pull from")
                return Status(returncode=1, changed=False)

            remote = remotes["upstream"]
            try:
                remote.fetch(callbacks=callbacks)
            except pygit2.GitError as exc:
                logger.error("    Failed to fetch '%s': %s", repo.name, exc)
                return Status(returncode=1, changed=False)

            remote_revision_id = git_repo.lookup_reference(
                f"refs/remotes/{remote.name}/{repo.revision}"
            ).target
            result, _ = git_repo.merge_analysis(remote_revision_id)

            if result & pygit2.GIT_MERGE_ANALYSIS_UP_TO_DATE:  # type: ignore[attr-defined]
                logger.info("    Repository for '%s' is up to date!", repo.name)
            elif result & pygit2.GIT_MERGE_ANALYSIS_FASTFORWARD:  # type: ignore[attr-defined]
                changed = True

                git_repo.checkout_tree(git_repo.get(remote_revision_id))  # type: ignore[attr-defined]
                ref = git_repo.lookup_reference(f"refs/heads/{repo.revision}")
                ref.set_target(remote_revision_id)
                git_repo.head.set_target(remote_revision_id)
                logger.info("    [bold]Repository for '%s' updated![/]", repo.name)
            else:
                logger.error("    [bold red]Fast-forwarding failed. Merge manually![/]")
                return Status(returncode=1, changed=False)

    update_submodules(git_repo)

    return Status(returncode=0, changed=changed)


def clone_repository(
    path: pathlib.Path,
    repo: Repository,
    *,
    callbacks: RemoteCallbacks,
) -> Status:
    try:
        git_repo = pygit2.clone_repository(  # type: ignore[no-untyped-call]
            repo.upstream,
            path=path / repo.name,
            checkout_branch=repo.revision,
            callbacks=callbacks,
        )
    except pygit2.GitError as exc:
        logger.error(
            "    Failed to clone repository '%s': %s", repo.name, exc, exc_info=exc
        )
        return Status(returncode=1, changed=False)
    except KeyError as exc:
        logger.error(
            "    Failed to clone repository '%s': %s", repo.name, exc, exc_info=exc
        )
        return Status(returncode=1, changed=False)

    # FIXME: guard this somehow? what errors can it return?
    update_submodules(git_repo)

    logger.info("    Creating 'upstream' remote '%s'", repo.upstream)
    git_repo.remotes.create("upstream", repo.upstream)
    git_repo.remotes.set_url("origin", repo.remote)

    return Status(returncode=0, changed=True)


def download_repository(
    path: pathlib.Path,
    repo: Repository,
    *,
    keyfile: str | None = None,
    quiet: bool = False,
) -> Status:
    repo_path = path / repo.name
    callbacks = RemoteCallbacks(keyfile=keyfile, quiet=quiet)

    if repo_path.exists():
        logger.info("    Repository '%s' already exists! Pulling.", repo.name)
        return pull_repository(path, repo, callbacks=callbacks)
    else:
        logger.info(
            "    Cloning repository '%s' (remote '%s')", repo.name, repo.upstream
        )
        return clone_repository(path, repo, callbacks=callbacks)


# }}}


# {{{ install projects


def build_backend(path: pathlib.Path) -> str | None:
    pyproject = path / "pyproject.toml"
    if pyproject.exists():
        import tomllib

        with open(pyproject, "rb") as f:
            project = tomllib.load(f)

        build_system = project.get("build-system")
        if build_system:
            backend = build_system.get("build-backend")
            if backend:
                backend, *_ = backend.split(".")

    # NOTE: if we didn't manage to find anything inside of `pyproject.toml`, just
    # assume that this is a legacy setuptools project if it has `setup.py`
    setuppy = path / "setup.py"
    if setuppy.exists():
        return "setuptools"

    return None


def get_build_dependencies(path: pathlib.Path) -> list[str]:
    from packaging.requirements import InvalidRequirement, Requirement

    def parse_requirement(text: str) -> str | None:
        try:
            requirement = Requirement(text.strip())
        except InvalidRequirement:
            return None

        if requirement.marker is None or requirement.marker.evaluate():
            return requirement.name

        return None

    dependencies = []

    requirementstxt = path / "requirements.txt"
    if requirementstxt.exists():
        with open(requirementstxt, encoding="utf-8") as f:
            for item in f:
                if item.startswith("#"):
                    continue

                dependencies.append(parse_requirement(item))

    pyprojecttoml = path / "pyproject.toml"
    if pyprojecttoml.exists():
        import tomllib

        with open(pyprojecttoml, "rb") as fb:
            info = tomllib.load(fb)

        build_system = info.get("build-system", {})
        for item in build_system.get("requires", []):
            dependencies.append(parse_requirement(item))

    return list({name for name in dependencies if name is not None})


def install_repository(
    path: pathlib.Path,
    repo: Repository,
    *,
    quiet: bool = True,
) -> int:
    path = path / repo.name

    if not path.exists():
        logger.error("    Repository path not found: %s", path)
        return 1

    # get dependencies
    dependencies = get_build_dependencies(path)
    logger.info("    Found package dependencies: %s", dependencies)

    # update environ to leave us alone.. this is a dev script
    env = os.environ.copy()
    if is_conda():
        # NOTE: pip complains that it's not in a venv when in a conda environment,
        # so this forces it to go ahead and do the install
        env["PIP_REQUIRE_VIRTUALENV"] = "false"

    backend_args = []
    if build_backend(path) == "setuptools":
        # FIXME: is this still needed in newer versions with `editable_mode=strict`?
        env["SETUPTOOLS_ENABLE_FEATURES"] = "legacy-editable"
        backend_args += ["--config-settings", "editable_mode=strict"]

    # https://pip.pypa.io/en/latest/user_guide/#using-pip-from-your-program
    import subprocess  # noqa: S404

    logger.info("    Installing repository '%s' (editable)", repo.name)
    stdout = subprocess.DEVNULL if quiet else None

    try:
        returncode = subprocess.check_call(  # noqa: S603
            [
                sys.executable,
                "-m",
                "pip",
                "install",
                "--verbose",
                "--no-build-isolation",
                *backend_args,
                *dependencies,
                "--editable",
                ".",
            ],
            cwd=path,
            env=env,
            stdout=stdout,
            stderr=stdout,
        )
    except subprocess.CalledProcessError as exc:
        returncode = exc.returncode

    if returncode:
        logger.error("    Failed to install project (use --show-output for details)")

    return returncode


# }}}


def is_venv() -> bool:
    # NOTE: reliable check for standard virtual environments
    #   https://stackoverflow.com/a/1883251
    return sys.prefix != sys.base_prefix


def is_conda() -> bool:
    # NOTE: conda sets up the environments such that the prefix is the same,
    # but we can check if we're in a conda env with variables
    #   https://stackoverflow.com/a/42660674
    return os.getenv("CONDA_DEFAULT_ENV", None) is not None


def download_project(
    path: pathlib.Path,
    project: Project,
    *,
    includes: list[str] | None = None,
    quiet: bool = True,
    no_install: bool = False,
    reinstall: bool = False,
) -> int:
    if not (is_venv() or is_conda()):
        logger.error("not inside a virtual (or conda) environment")
        return 1

    path = path.resolve()
    if not path.exists():
        logger.info("project path '%s' does not exist. Creating ...", path)
        path.mkdir()

    if not path.is_dir():
        logger.error("'path' is not a directory: '%s'", path)
        return 1

    set_includes = {} if includes is None else set(includes)
    returncode = 0
    for repo in project.repos.values():
        if includes is not None and repo.name not in set_includes:
            continue

        logger.info("Processing '%s' repository at '%s'", repo.name, path / repo.name)
        status = download_repository(
            path,
            repo,
            keyfile=project.keyfile,
            quiet=quiet,
        )
        returncode &= status.returncode

        if not no_install and status.returncode == 0 and (reinstall or status.changed):
            r = install_repository(path, repo, quiet=quiet)
            returncode &= r

    return returncode


def list_project(project: Project) -> int:
    logger.info("Project '%s' (%d repositories)", project.name, len(project.repos))

    width_name = width_remote = 2
    for repo in project.repos.values():
        width_name = max(width_name, len(repo.name) + 2)
        width_remote = max(width_remote, len(repo.upstream) + 2)

    fmt = f"    Repository %-{width_name}s: %-{width_remote}s (revision '%s')"
    for repo in project.repos.values():
        logger.info(fmt, f"'{repo.name}'", f"'{repo.upstream}'", repo.revision)

    return 0


if __name__ == "__main__":
    import argparse

    import shtab

    class HelpFormatter(
        argparse.ArgumentDefaultsHelpFormatter,
        argparse.RawDescriptionHelpFormatter,
    ):
        pass

    choices = get_from_config(SCRIPT_CONFIG_FILE)
    if not choices:
        logger.error("No valid projects configured in '%s'.", SCRIPT_CONFIG_FILE)
        raise SystemExit(1)

    parser = argparse.ArgumentParser(
        formatter_class=HelpFormatter,
        description=SCRIPT_LONG_HELP,
    )
    shtab.add_argument_to(parser, ["--print-completion"])
    parser.add_argument(  # type: ignore[attr-defined]
        "path",
        nargs="*",
        default=pathlib.Path.cwd(),
        type=pathlib.Path,
        help="install path for all the project repos",
    ).complete = shtab.DIRECTORY
    parser.add_argument(
        "-p",
        "--project",
        choices=list(choices),
        default=next(iter(choices)),
        help="name of the project to download",
    )
    parser.add_argument(
        "--only",
        default=None,
        help="Only update the given repositories (if they exist)",
    )
    parser.add_argument(
        "--no-install",
        action="store_true",
        help="only download repositories (do not 'pip install')",
    )
    parser.add_argument(
        "-R",
        "--reinstall",
        action="store_true",
        help="force a 'pip install --editable'",
    )
    parser.add_argument(
        "-l",
        "--list",
        action="store_true",
        help="list all repositories in the project (without downloading)",
    )
    parser.add_argument(
        "-q",
        "--quiet",
        action="store_true",
        help="suppress script output",
    )
    parser.add_argument(
        "--show-output",
        action="store_true",
        help="show output from 'pip' and friends",
    )
    args = parser.parse_args()

    if not args.quiet:
        logger.setLevel(logging.INFO)

    if args.list:
        raise SystemExit(list_project(choices[args.project]))
    else:
        if len(args.path) != 1:
            logger.error("Can only pass one path: %s", args.path)
            raise SystemExit(1)

        raise SystemExit(
            download_project(
                args.path[0],
                choices[args.project],
                includes=None if args.only is None else args.only.split(" "),
                no_install=args.no_install,
                reinstall=args.reinstall,
                quiet=not args.show_output,
            )
        )
