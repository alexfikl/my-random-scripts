#! /usr/bin/env python3

# SPDX-FileCopyrightText: 2023 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from __future__ import annotations

import logging
import pathlib
from dataclasses import dataclass, replace

import platformdirs
import rich.logging

# {{{ script

logger = logging.getLogger(pathlib.Path(__file__).stem)
logger.setLevel(logging.ERROR)
logger.addHandler(rich.logging.RichHandler(markup=True))

SCRIPT_PATH = pathlib.Path(__file__)
SCRIPT_CONFIG_DIR = platformdirs.user_config_path("yorick.scripts.com")
SCRIPT_CONFIG_FILE = SCRIPT_CONFIG_DIR / "branches.toml"
SCRIPT_LONG_HELP = f"""\
A script that handles switching branches in multiple repositories.

The script uses the configuration file

    {SCRIPT_CONFIG_FILE}

that defines a root folder with repositories (called a workgroup) and a set of
projects that match each repository to a particular branch. This essentially
allows switching branches in multiple repos as if they were a monorepo.

The configuration format is as follows

    [<WORKGROUP_ID>]
    root = <PATH_TO_REPOSITORIES>
    excludes = [<LIST_OF_EXCLUDED_REPOS>]

    [<WORKGROUP_ID>.projects.<PROJECT_ID>]
    <REPO_NAME_1> = <BRANCH_NAME_1>
    <REPO_NAME_2> = <BRANCH_NAME_2>

Example:

    > {SCRIPT_PATH.name} --list
    > {SCRIPT_PATH.name} -w <WID> -p <PID>
"""

# }}}


# {{{ utils


def get_default_branch(path: pathlib.Path) -> str:
    import pygit2

    # FIXME: figure out how to get the default branch
    try:
        repo = pygit2.Repository(path)
        for branch in repo.branches.local:  # type: ignore[attr-defined]
            name = str(branch)
            if name in {"main", "master"}:
                return name
    except pygit2.GitError as exc:
        logger.error("Failed to access repository '%s'.", path, exc_info=exc)

    return "main"


# }}}


# {{{ get repos


@dataclass(frozen=True)
class Repository:
    #: Path to the ``git`` repository.
    path: pathlib.Path
    #: Branch to which to switch to. If *None* or given branch is already *HEAD*,
    #: no checkout is performed (which is marked by :attr:`switch`).
    to_branch: str
    #: If *True*, this branch was switch to from another branch.
    switch: bool

    @property
    def name(self) -> str:
        """Name of the repository."""
        return self.path.name


def get_known_repos(
    root: pathlib.Path,
    *,
    excludes: frozenset[str] | None = None,
    default_branch: str | None = None,
) -> dict[str, Repository]:
    if excludes is None:
        excludes = frozenset()

    known_repos = {}
    for path in root.iterdir():
        if not path.is_dir():
            continue

        if path.name in excludes:
            continue

        branch = default_branch
        if branch is None:
            branch = get_default_branch(path)

        logger.debug("%s: default branch '%s'.", path.name, branch)
        known_repos[path.name] = Repository(path=path, to_branch=branch, switch=False)

    return known_repos


# }}}


# {{{ get projects


@dataclass(frozen=True)
class Project:
    #: Name of the project.
    name: str
    #: A list of repositories in this project.
    repos: dict[str, Repository]


@dataclass(frozen=True)
class Workgroup:
    #: Name of the workgroup.
    name: str
    #: Root path to all the projects in this workgroup.
    root: pathlib.Path
    #: A list of projects to exclude from the workgroup.
    excludes: frozenset[str]
    #: A mapping of names to projects in the workgroup.
    projects: dict[str, Project]


def get_from_config(path: pathlib.Path) -> dict[str, Workgroup]:
    if not path.exists():
        logger.error("Configuration file does not exist: '%s'.", path)

    import tomllib

    with open(path, "rb") as f:
        settings = tomllib.load(f)

    workgroups = {}
    for wid in settings:
        wg = settings[wid]
        if "projects" not in wg:
            logger.error("Workgroup '%s' has no projects defined.", wid)
            continue

        root = pathlib.Path(wg.get("root", ".")).expanduser().resolve()
        if not root.exists():
            logger.error("Workgroup '%s' root does not exist: '%s'", wid, root)
            continue

        excludes = frozenset([str(r) for r in wg.get("excludes", [])])
        known_repos = get_known_repos(root, excludes=excludes, default_branch=None)

        projects = {}
        for pid, branches in wg["projects"].items():
            repos = known_repos.copy()

            for rid, branch in branches.items():
                if rid not in repos:
                    logger.error("Repository '%s' is not in '%s'.", rid, root)
                    continue

                repos[rid] = replace(repos[rid], to_branch=branch, switch=True)

            projects[pid] = Project(name=pid, repos=repos)

        workgroups[wid] = Workgroup(
            name=wid, root=root, excludes=excludes, projects=projects
        )

    return workgroups


# }}}


# {{{ switch branches


def switch_branch_in_project(project: Project, *, stash: bool = True) -> int:
    import pygit2

    for r in project.repos.values():
        repo = pygit2.Repository(r.path)
        if repo.head.shorthand == r.to_branch:
            logger.debug("[bold white]Processing '%s'[/]", r.path)
            logger.debug(
                "    Repository '%s' already on branch '%s'", r.path.parent, r.to_branch
            )
            continue

        logger.info("[bold white]Processing '%s'[/]", r.path)
        has_changes = len(list(repo.diff().deltas)) > 0  # type: ignore[attr-defined]
        if has_changes:
            if stash:
                logger.info("    Stashing changes in '%s'", r.path.parent)
                repo.stash(repo.default_signature, "[pygit2] stashed for switch")  # type: ignore[attr-defined]
            else:
                logger.error("    Repository '%s' has uncommitted changes", r.name)
                return 1

        logger.info(
            "    Changing branch to '%s' (from '%s')", r.to_branch, repo.head.shorthand
        )

        branch = repo.lookup_branch(r.to_branch)
        ref = repo.lookup_reference(branch.name)
        repo.checkout(ref)  # type: ignore[attr-defined]

    return 0


# }}}


# {{{ list projects


def list_projects(workgroups: dict[str, Workgroup]) -> int:
    for wg in workgroups.values():
        logger.info("[bold white]Workgroup[/]: %s", wg.name)

        first_repo = True
        for project in wg.projects.values():
            if first_repo:
                first_repo = False
                logger.info("[bold white]Repositories[/]: %s", list(project.repos))

            all_main = True
            logger.info("[bold white]Project[/]: '%s'", project.name)
            for repo in project.repos.values():
                if not repo.switch:
                    continue

                all_main = False
                logger.info(
                    "    Repository '%s' branch '%s'", repo.name, repo.to_branch
                )

            if all_main:
                logger.info("    All repositories on default branch")

    return 0


# }}}


if __name__ == "__main__":
    import argparse

    import shtab

    class HelpFormatter(
        argparse.ArgumentDefaultsHelpFormatter,
        argparse.RawDescriptionHelpFormatter,
    ):
        pass

    workgroups = get_from_config(SCRIPT_CONFIG_FILE)
    if not workgroups:
        logger.error("No valid workgroups are configured in '%s'.", SCRIPT_CONFIG_FILE)
        raise SystemExit(1)

    import operator
    from functools import reduce

    projects = sorted(
        reduce(operator.or_, [set(wg.projects) for wg in workgroups.values()])
    )

    parser = argparse.ArgumentParser(
        formatter_class=HelpFormatter,
        description=SCRIPT_LONG_HELP,
    )
    shtab.add_argument_to(parser, ["--print-completion"])
    parser.add_argument(
        "-w",
        "--workgroup",
        choices=list(workgroups),
        default=next(iter(workgroups)),
        help="name of the workgroup",
    )
    parser.add_argument(
        "-p",
        "--project",
        choices=list(projects),
        default="main",
        help="name of the project to switch to",
    )
    parser.add_argument(
        "--stash",
        action="store_true",
        help="git stash project changes before switching branches",
    )
    parser.add_argument(
        "--list",
        action="store_true",
        help="list all the workgroups and projects",
    )
    parser.add_argument(
        "-q",
        "--quiet",
        action="store_true",
        help="only show error messages",
    )
    args = parser.parse_args()

    if not args.quiet:
        logger.setLevel(logging.INFO)

    if args.list:
        raise SystemExit(list_projects(workgroups))
    else:
        raise SystemExit(
            switch_branch_in_project(
                workgroups[args.workgroup].projects[args.project],
                stash=args.stash,
            )
        )
