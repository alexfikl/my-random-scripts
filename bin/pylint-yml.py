#!/usr/bin/env python

# SPDX-FileCopyrightText: 2023 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from __future__ import annotations

import pathlib
from collections.abc import Iterator

import platformdirs
import yaml

CI_URL = "https://raw.githubusercontent.com/inducer/ci-support/main"
CI_DEFAULT_RCFILE = ".pylintrc-default.yml"

YAML_DEFAULT_RCFILE = pathlib.Path(platformdirs.user_config_dir()) / "pylintrc.yml"
YAML_LOCAL_RCFILE = ".pylintrc-local.yml"


def generate_args_from_yaml(filename: pathlib.Path | str) -> Iterator[str]:
    """Generate a list of strings suitable for use as pylint arguments from
    a YAML file.

    :arg input_yaml: YAML data, as an input file or bytes.
    """

    with open(filename, encoding="utf-8") as infile:
        for entry in yaml.safe_load(infile):
            arg = entry["arg"]
            val = entry.get("val")

            if val is not None:
                if isinstance(val, list):
                    val = ",".join([str(item) for item in val])

                yield f"--{arg}={val}"
            else:
                yield f"--{arg}"


def run_pylint() -> int:
    args = ["--jobs=4", "--recursive", "y"]
    excluded_dirs: set[str] = set()

    # {{{ handle command line

    import sys

    rcfile_prefix = "--yaml-rcfile="
    is_print_completion = False
    for arg in sys.argv[1:]:
        if arg == "--print-completion":
            is_print_completion = True
            break

        if arg.startswith(rcfile_prefix):
            config_path = arg[len(rcfile_prefix) :]
            args.extend(generate_args_from_yaml(config_path))
        elif arg.startswith("--exclude-dirs="):
            _, dirs = arg.split("=")
            excluded_dirs = excluded_dirs | set(dirs.split(","))
        else:
            args.append(arg)

    if is_print_completion:
        return 0

    # }}}

    # {{{ add default rcfile

    if not YAML_DEFAULT_RCFILE.exists():
        from urllib.request import Request, urlopen

        req = Request(  # noqa: S310
            f"{CI_URL}/{CI_DEFAULT_RCFILE}", headers={"User-Agent": "pylint.py"}
        )

        with urlopen(req) as url:  # noqa: S310
            rc = url.read()
            with open(YAML_DEFAULT_RCFILE, "w", encoding="utf-8") as outfile:
                outfile.write(rc.decode())

    args.extend(generate_args_from_yaml(YAML_DEFAULT_RCFILE))

    # }}}

    # {{{ add local rcfile

    cwd = pathlib.Path.cwd()
    rcfile = cwd / YAML_LOCAL_RCFILE

    if rcfile.exists():
        args.extend(generate_args_from_yaml(rcfile))

    # enable any other warnings we want to force
    args.extend([
        "--enable=I0021",
    ])

    # }}}

    # {{{ add directories to lint

    args.extend([
        name
        for name in [cwd.name, "examples", "test", "tests"]
        if name not in excluded_dirs and (cwd / name).exists()
    ])

    # }}}

    import warnings

    import pylint.lint

    with warnings.catch_warnings():
        print("pylint {}".format(" ".join(args)))
        warnings.simplefilter("ignore")
        r = pylint.lint.Run(args, exit=False)

    return r.linter.msg_status


if __name__ == "__main__":
    raise SystemExit(run_pylint())
