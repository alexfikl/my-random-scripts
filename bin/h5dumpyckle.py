#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2023 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from __future__ import annotations

import logging
import pathlib

import h5py
import rich.logging

# {{{ script

logger = logging.getLogger(pathlib.Path(__file__).stem)
logger.setLevel(logging.ERROR)
logger.addHandler(rich.logging.RichHandler())

SCRIPT_PATH = pathlib.Path(__file__)
SCRIPT_LONG_HELP = f"""\
Dump structure of HDF5 files written by h5pyckle <https://github.com/alexfikl/h5pyckle>.

This is meant to show the type information stored by h5pyckle in a helpful way
that is similar to h5dump. For more advanced usage, h5dump can be used directly.

Example:

    > {SCRIPT_PATH.name} --group <GROUP_NAME> <HDF5_FILE>
"""
# }}}


# {{{ display

_GROUP_S = "\033[1;31mGROUP\033[0m"
_ATTRIBUTE_S = "\033[1;33mATTRIBUTE\033[0m"
_DATASET_S = "\033[1;32mDATASET\033[0m"
_CLASS_S = "\033[1;37mCLASS\033[0m"


def show_dataset(dset: h5py.Dataset, indent: int = 0) -> None:
    pre_indent = "  " * indent
    post_indent = "  " * (indent + 1)

    name = dset.name.split("/")[-1]
    print(f'{pre_indent}{_DATASET_S} "{name}" {{')
    print(f"{post_indent}DATATYPE {dset.dtype}")
    print(f"{post_indent}DATASPACE SIMPLE {{ {dset.shape} / {dset.maxshape} }}")
    print(f"{pre_indent}}}")


def show_pickle_group(grp: h5py.File | h5py.Group, indent: int = 0) -> None:
    pre_indent = "  " * indent
    post_indent = "  " * (indent + 1)

    from h5pyckle import PickleGroup

    grp = PickleGroup.from_h5(grp)

    name = grp.name.split("/")[-1] or "/"
    print(f'{pre_indent}{_GROUP_S} "{name}" {{')
    if grp.has_type:
        type_name = grp.attrs["__type_name"]
        print(f'{post_indent}{_CLASS_S}: "{type_name}"')

    from h5pyckle import load_from_attribute
    from h5pyckle.base import _H5PYCKLE_RESERVED_ATTRS  # noqa: PLC2701

    for key in grp.attrs.keys():  # noqa: SIM118
        if key in _H5PYCKLE_RESERVED_ATTRS:
            continue

        value = load_from_attribute(key, grp)
        print(f'{post_indent}{_ATTRIBUTE_S} "{key}": {value}')

    for key in grp.keys():  # noqa: SIM118
        if isinstance(grp[key], h5py.Group):
            show_pickle_group(grp[key], indent=indent + 1)
        else:
            show_dataset(grp[key], indent=indent + 1)

    print(f"{pre_indent}}}")


# }}}


def dump(infile: pathlib.Path, group: str) -> int:
    try:
        import h5pyckle  # noqa: F401
    except ImportError:
        logger.error("This script requires the 'h5pyckle' package")
        return 1

    with h5py.File(infile, "r") as h5:
        if group not in {"", "/"}:
            if group not in h5:
                logger.error("Group '%s' not in '%s'", group, infile)
                return 1

            grp = h5[group]
        else:
            grp = h5["/"]

        if "__version" in grp.attrs:
            version = grp.attrs["__version"]
            print(f'HDF5 "{infile.name}" H5PYCKLE_VERSION "{version}" {{')
        else:
            print(f'HDF5 "{infile.name}" {{')

        show_pickle_group(grp)

        print("}")

    return 0


if __name__ == "__main__":
    import argparse

    import shtab

    class HelpFormatter(
        argparse.ArgumentDefaultsHelpFormatter,
        argparse.RawDescriptionHelpFormatter,
    ):
        pass

    HDF_FILE = {"zsh": "_files -g '(*.h5|*.hdf)'"}

    parser = argparse.ArgumentParser(
        formatter_class=HelpFormatter,
        description=SCRIPT_LONG_HELP,
    )
    shtab.add_argument_to(parser, ["--print-completion"])
    parser.add_argument(  # type: ignore[attr-defined]
        "filename", type=pathlib.Path
    ).complete = HDF_FILE
    parser.add_argument("-g", "--group", default="")
    parser.add_argument(
        "-q",
        "--quiet",
        action="store_true",
        help="only show error messages",
    )
    args = parser.parse_args()

    if not args.quiet:
        logger.setLevel(logging.INFO)

    raise SystemExit(dump(args.filename, args.group))
