#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2023 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from __future__ import annotations

import logging
import pathlib

import rich.logging

# {{{ script

logger = logging.getLogger(pathlib.Path(__file__).stem)
logger.setLevel(logging.ERROR)
logger.addHandler(rich.logging.RichHandler())

SCRIPT_PATH = pathlib.Path(__file__)
SCRIPT_LONG_HELP = f"""\
Simple wrapper around ffmpeg to convert images into a movie.

This takes care a bit of the padding of the images so that everything is
of the right size. It also supports some default formats with settings for
good quality.

Example:

    > {SCRIPT_PATH.name} --framerate 24 --output output.mp4 <IMAGES...>
"""

# }}}


def make_movie(
    pattern: str,
    output: pathlib.Path,
    *,
    framerate: int = 30,
    pattern_type: str = "sequence",
) -> int:
    try:
        from ffmpeg import FFmpeg, Progress
    except ImportError:
        logger.error("This script requires the 'python-ffmpeg' Python bindings.")
        return 1

    if output.suffix == ".mp4":
        vcodec = "libx264"
    elif output.suffix == ".webp":
        vcodec = "libwebp"
    else:
        return 1

    if output.exists():
        logger.warning("Overwriting output file '%s'.", output)
        output.unlink()

    process = (
        FFmpeg()
        .option("framerate", framerate)
        .input(pattern, f="image2", pattern_type=pattern_type)
        .option("filter_complex", "pad=ceil(iw/2)*2:ceil(ih/2)*2:0:0")
        .output(str(output), pix_fmt="yuv420p", vcodec=vcodec)
    )

    @process.on("progress")  # type: ignore[misc]
    def on_progress(progress: Progress) -> None:
        print(progress)

    process.execute()

    return 0


if __name__ == "__main__":
    import argparse

    import shtab

    class HelpFormatter(
        argparse.ArgumentDefaultsHelpFormatter,
        argparse.RawDescriptionHelpFormatter,
    ):
        pass

    parser = argparse.ArgumentParser(
        formatter_class=HelpFormatter,
        description=SCRIPT_LONG_HELP,
    )
    shtab.add_argument_to(parser, ["--print-completion"])
    parser.add_argument("pattern", help="filename pattern for input image sequence")
    parser.add_argument("-f", "--framerate", type=int, default=25)
    parser.add_argument(
        "--pattern-type",
        choices=["sequence", "glob", "none"],
        default="sequence",
        help="Selects pattern type used when interpreting the filename",
    )
    parser.add_argument(  # type: ignore[attr-defined]
        "-o",
        "--output",
        default="output.mp4",
        type=pathlib.Path,
        help="output file name",
    ).complete = shtab.FILE
    args = parser.parse_args()

    raise SystemExit(
        make_movie(
            args.pattern,
            args.output,
            pattern_type=args.pattern_type,
            framerate=args.framerate,
        )
    )
