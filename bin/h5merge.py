#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2023 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from __future__ import annotations

import logging
import pathlib

import h5py
import rich.logging

# {{{ script

logger = logging.getLogger(pathlib.Path(__file__).stem)
logger.setLevel(logging.ERROR)
logger.addHandler(rich.logging.RichHandler())

SCRIPT_PATH = pathlib.Path(__file__)
SCRIPT_LONG_HELP = f"""\
A script to merge multiple HDF5 files.

This has a very limited used, since it does not try to merge datasets in any
way. The intended application was to merge time series scattered across different
files. For example, if one file has groups

    CHECKPOINT_0001
    CHECKPOINT_0002
    CHECKPOINT_0003

and another one continues at

    CHECKPOINT_0004
    CHECKPOINT_0005
    CHECKPOINT_0006

this scrip can merge the two together into a single file for easier storage and
manipulation.

Example:

    > {SCRIPT_PATH.name} <FILE_1.h5> <FILE_2.h5> <FILE_3.h5>
"""

# }}}


# {{{ merge


def copy_attrs(igrp: h5py.Group, ogrp: h5py.Group) -> None:
    if not isinstance(igrp, h5py.Group):
        return

    if igrp.attrs:
        ogrp.attrs.update(igrp.attrs)

    for isubgrp, osubgrp in zip(igrp.values(), ogrp.values(), strict=True):
        assert isubgrp.name == osubgrp.name
        copy_attrs(isubgrp, osubgrp)


def h5_merge(infiles: list[pathlib.Path], outfile: pathlib.Path | None = None) -> int:
    if outfile is None:
        outfile = pathlib.Path("out.h5").resolve()

    # NOTE: hack to empty the file? necessary?
    # FIXME: use temporary files and copy at the end!

    logger.info("Merging into '%s'...", outfile)
    with h5py.File(outfile, mode="w") as outf:
        pass

    with h5py.File(outfile, mode="a") as outf:  # noqa PLR1702
        unique_infile_groups: dict[pathlib.Path, set[str]] = {}
        for infile in infiles:
            if not infile.exists():
                logger.error("Input file does not exist: '%s'", infile)
                return 1

            logger.info("Processing '%s'...", infile)
            with h5py.File(infile, mode="r") as inf:
                for key in inf:
                    found = False
                    for f, groups in unique_infile_groups.items():
                        if key not in groups:
                            continue

                        logger.info(
                            "Group '%s' is duplicated (previously in '%s')",
                            key,
                            f,
                        )
                        found = True
                        break

                    if found:
                        continue

                    # NOTE: setting `without_attrs=False` seems to make hdf5
                    # crash with a segmentation fault; not sure why
                    inf.copy(inf[key], outf, without_attrs=True)
                    copy_attrs(inf[key], outf[key])

                unique_infile_groups[infile] = inf.keys()

    return 0


# }}}


if __name__ == "__main__":
    import argparse

    import shtab

    class HelpFormatter(
        argparse.ArgumentDefaultsHelpFormatter,
        argparse.RawDescriptionHelpFormatter,
    ):
        pass

    HDF_FILE = {"zsh": "_files -g '(*.h5|*.hdf)'"}

    parser = argparse.ArgumentParser(
        formatter_class=HelpFormatter,
        description=SCRIPT_LONG_HELP,
    )
    shtab.add_argument_to(parser, ["--print-completion"])
    parser.add_argument(  # type: ignore[attr-defined]
        "inputs",
        nargs="+",
        type=pathlib.Path,
        help="input files to merge into a single HDF5 file",
    ).complete = HDF_FILE
    parser.add_argument(  # type: ignore[attr-defined]
        "-o",
        "--output",
        type=pathlib.Path,
        default="out.h5",
        help="name of merged file",
    ).complete = shtab.FILE
    parser.add_argument(
        "-q",
        "--quiet",
        action="store_true",
        help="only show error messages",
    )
    args = parser.parse_args()

    if not args.quiet:
        logger.setLevel(logging.INFO)

    raise SystemExit(h5_merge(args.inputs, args.output))
