#!/usr/bin/env python

# SPDX-FileCopyrightText: 2023 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from __future__ import annotations

import logging
import pathlib

import rich.logging

# {{{ script

logger = logging.getLogger(pathlib.Path(__file__).stem)
logger.setLevel(logging.ERROR)
logger.addHandler(rich.logging.RichHandler())

SCRIPT_PATH = pathlib.Path(__file__)
SCRIPT_LONG_HELP = f"""\
A script that determines the current architecture level of the machine.
This is based on the instruction set available, as described on Wikipedia:
https://en.wikipedia.org/wiki/X86-64#Microarchitecture_levels

Briefly, the available levels are:

    * Level 1: has SSE2
    * Level 2: has SSE4
    * Level 3: Has AVX2
    * Level 4: Has AVX512

Example:

    > {SCRIPT_PATH.name} --quiet
    x86-64-v3
"""

# }}}


# {{{ main

# https://en.wikipedia.org/wiki/X86-64#Microarchitecture_levels
ARCH_V1_FLAGS = {"lm", "cmov", "cx8", "fpu", "fxsr", "mmx", "syscall", "sse2"}
ARCH_V2_FLAGS = {"cx16", "lahf_lm", "popcnt", "sse4_1", "sse4_2", "ssse3"}
ARCH_V3_FLAGS = {"avx", "avx2", "bmi1", "bmi2", "f16c", "fma", "abm", "movbe", "xsave"}
ARCH_V4_FLAGS = {"avx512f", "avx512bw", "avx512cd", "avx512dq", "avx512vl"}

ARCH_FLAGS = [
    (4, ARCH_V4_FLAGS),
    (3, ARCH_V3_FLAGS),
    (2, ARCH_V2_FLAGS),
    (1, ARCH_V1_FLAGS),
]


def get_gcc_march_linux() -> str:
    with open("/proc/cpuinfo", encoding="utf-8") as f:
        cpuinfo = f.read()

    (all_flags,) = {
        line.split(":")[-1].strip()
        for line in cpuinfo.split("\n")
        if line.startswith("flags")
    }
    flags = set(all_flags.split(" "))

    for i, arch in ARCH_FLAGS:
        if not arch - flags:
            return f"x86-64-v{i}"

    return "x86-64"


def main() -> int:
    import platform

    system = platform.system()
    if system == "Linux":
        arch = get_gcc_march_linux()
    else:
        logger.error("Unsupported platform: '%s'", system)
        return 1

    print(arch)
    return 0


# }}}


if __name__ == "__main__":
    import argparse

    import shtab

    class HelpFormatter(
        argparse.ArgumentDefaultsHelpFormatter,
        argparse.RawDescriptionHelpFormatter,
    ):
        pass

    parser = argparse.ArgumentParser(
        formatter_class=HelpFormatter,
        description=SCRIPT_LONG_HELP,
    )
    shtab.add_argument_to(parser, ["--print-completion"])
    parser.add_argument(
        "-q",
        "--quiet",
        action="store_true",
        help="only show error messages",
    )
    args = parser.parse_args()

    if not args.quiet:
        logger.setLevel(logging.INFO)

    raise SystemExit(main())
