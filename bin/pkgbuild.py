#!/usr/bin/env python

# SPDX-FileCopyrightText: 2023 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

# TODO:
# - parse `makepkg.conf` or something to get defaults?
# - split packages?
# - `fakeroot`?

from __future__ import annotations

import logging
import os
import pathlib
import shutil
import subprocess  # noqa: S404
from dataclasses import dataclass
from typing import Any, TextIO

import platformdirs
import rich.logging

# {{{ script

logger = logging.getLogger(pathlib.Path(__file__).stem)
logger.setLevel(logging.ERROR)
logger.addHandler(rich.logging.RichHandler())

SCRIPT_PATH = pathlib.Path(__file__)
SCRIPT_CONFIG_DIR = platformdirs.user_config_path("yorick.scripts.com")
SCRIPT_CONFIG_FILE = SCRIPT_CONFIG_DIR / "pkgbuild.toml"
SCRIPT_LONG_HELP = f"""\
Helper script to build packages on Arch Linux.

This script runs `updpkgsums`, `makepkg` and ultimately copies the resulting
package into a local repository. It largely performs the following steps
1. Update the `sha512sums` using `updpkgsums`;
2. Build the package using `makepkg`;
3. Generate `SRCINFO` files;
4. Add the signed package to a repository using `repo-add`;
5. Check the resulting package with `namcap`.

The script has some configuration at

    {SCRIPT_CONFIG_FILE}

which can configure multiple repositories as

    [<repository-name>]
    key = <gpg-key-name>
    path = <path/to/repository>
    namcap = <true/false>
    buildbase = <directory-name>
    ext = <package-extension>

Example:

    > {SCRIPT_PATH.name} some-package/PKGBUILD
"""
# }}}


# {{{ utils


@dataclass(frozen=True)
class Repository:
    #: A name for the repository (as it shows up in ``makepkg.conf``).
    name: str
    #: A local path to the repository.
    path: pathlib.Path
    #: A GPG key used to sign the packages.
    key: str | None

    #: A (absolute or relative) directory in which the packages are built.
    buildbase: pathlib.Path
    #: If *True*, run ``namcap`` after the package is built.
    namcap: bool
    #: Package extension (e.g. ``zst``) as set in ``makepkg.conf``.
    ext: str

    @property
    def db(self) -> pathlib.Path:
        return self.path / f"{self.name}.db.tar{self.ext}"


@dataclass(frozen=True)
class Package:
    #: Name of the package, i.e. ``pkgname``.
    name: str
    #: Version of the package, i.e. ``pkgver``.
    version: str
    #: A path to the package archive in :attr:`Repository.buildbase`.
    path: pathlib.Path


def call(
    cmd: str,
    cargs: tuple[Any, ...],
    *,
    cwd: pathlib.Path | None = None,
    outfile: pathlib.Path | None = None,
    environ: dict[str, Any] | None = None,
    quiet: bool = False,
) -> int:
    cmd_exists = shutil.which(cmd) is not None
    if not cmd_exists:
        logger.error("Executable '%s' does not exist on path", cmd)
        return 1

    stdout: int | TextIO | None = None
    if outfile is not None:
        stdout = open(outfile, "w", encoding="utf-8")  # noqa: SIM115
    else:
        stdout = subprocess.DEVNULL if quiet else None
    stderr = subprocess.DEVNULL if quiet else None

    if environ is not None:
        environ = {**os.environ, **environ}

    try:
        process = subprocess.run(  # noqa: S603
            [cmd] + [str(c) for c in cargs],
            cwd=cwd,
            stdout=stdout,
            stderr=stderr,
            env=environ,
            check=True,
        )
        returncode = process.returncode
    except subprocess.CalledProcessError as exc:
        logger.error("Command '%s' exited with code: '%s'", cmd, exc.returncode)
        returncode = exc.returncode

    if outfile is not None:
        stdout.close()  # type: ignore[union-attr]

    return returncode


def get_from_config(path: pathlib.Path) -> dict[str, Repository]:
    if not path.exists():
        logger.error("Configuration file does not exist: '%s'", path)
        return {}

    import tomllib

    with open(path, "rb") as f:
        settings = tomllib.load(f)

    repos = {}
    for rid, options in settings.items():
        tmp = options.get("path")
        if tmp is None:
            logger.error("Repository '%s' is missing a 'path'. Skipping", rid)
            continue

        path = pathlib.Path(tmp)
        if not path.exists():
            logger.error("Path for repository '%s' does not exist: %s", rid, path)
            continue

        if not path.is_dir():
            logger.error("Path for repository '%s' is not a directory: %s", rid, path)
            continue

        repos[rid] = Repository(
            name=rid,
            path=path,
            key=options.get("key"),
            buildbase=pathlib.Path(options.get("buildbase", "_build")),
            namcap=options.get("namcap", False),
            ext=".{}".format(options.get("ext", "zst")),
        )

    return repos


# }}}


# {{{ makepkg


def make(
    repo: Repository,
    pkgbuild: pathlib.Path,
    *,
    repackage: bool = False,
    noextract: bool = False,
    quiet: bool = False,
) -> tuple[Package, ...]:
    try:
        import srcinfo.parse as srcinfo
    except ImportError:
        logger.error("This script requires the 'python-srcinfo' package")
        return ()

    cwd = pkgbuild.parent
    pkgbuild_filename = pkgbuild.name
    builddir = cwd / repo.buildbase

    if not builddir.exists():
        builddir.mkdir()

    # copy all files so we can build in builddir
    for filename in cwd.iterdir():
        if filename.name == "_build" or filename.name[0] == ".":
            continue

        shutil.copy2(filename, builddir / filename.name)

    # {{{ make sure sha sums are up to date if possible

    logger.info("Updating SHA sums...")

    returncode = call(
        "updpkgsums",
        (pkgbuild_filename,),
        cwd=builddir,
        quiet=quiet,
    )

    if returncode != 0:
        logger.error("Could not update SHA sums")

    # }}}

    # {{{ build package

    logger.info("Building package. This may take a while...")

    cargs: tuple[Any, ...] = ()
    if repackage:
        cargs = (*cargs, "--repackage")

    if noextract:
        cargs = (*cargs, "--noextract")

    if quiet:
        cargs = (*cargs, "--noconfirm")

    cargs = (
        *cargs,
        "--syncdeps",
        "--force",
        "--nocheck",
        "--rmdeps",
        "-p",
        pkgbuild_filename,
    )

    if repo.key is not None:
        cargs = (*cargs, "--sign", "--key", repo.key)

    returncode = call("makepkg", cargs, cwd=builddir, quiet=quiet)

    if returncode != 0:
        if not quiet:
            call(
                "makepkg",
                ("--geninteg", "-p", pkgbuild_filename),
                cwd=builddir,
                quiet=quiet,
            )

        logger.error("'makepkg' exited with code '%d'", returncode)
        return ()

    # # NOTE: PKGBUILD was changed by updpkgsums or pkgver, so copy it back
    shutil.copy2(builddir / pkgbuild_filename, cwd / pkgbuild_filename)

    # }}}

    # {{{ generate srcinfo

    logger.info("Generating SRCINFO...")

    outfile = cwd / ".SRCINFO"
    returncode = call(
        "makepkg",
        ("--printsrcinfo", "-p", pkgbuild_filename),
        cwd=builddir,
        outfile=outfile,
        quiet=quiet,
    )

    if returncode != 0:
        logger.error("Failed to generated .SRCINFO file.")
        return ()

    with open(outfile, encoding="utf-8") as f:
        p, errors = srcinfo.parse_srcinfo(f.read())

    if errors:
        logger.error("Failed to parse .SRCINFO file")
        return ()

    if len(p["packages"]) != 1:
        logger.error("Cannot build split packages")
        return ()

    # }}}

    # find which package was actually built
    filenames = sorted([f for f in builddir.iterdir() if f.suffix == repo.ext])

    nfiles = len(filenames)
    if nfiles == 0:
        logger.error("No package files were found with extension '%s'", repo.ext)
        return ()

    if nfiles == 1:
        pkgpath = filenames[0]
    else:
        if quiet:
            logger.error("Multiple package files were found. Choosing the first one!")
            index = 0
        else:
            import rich.prompt

            logger.error(
                "Multiple package files were found. Choose one below ...\n%s",
                "\n".join(f"{i}. '{f.name}'" for i, f in enumerate(filenames)),
            )

            index = -1
            while index < 0 or index >= nfiles:
                # NOTE: this is indented to the current rich log formatter
                index = rich.prompt.IntPrompt.ask(
                    f"                             Choose a file [0 - {nfiles - 1}]",
                    default=0,
                )

        pkgpath = filenames[index]

    pkg = Package(
        name=p["pkgbase"],
        version="{}-{}".format(p["pkgver"], p["pkgrel"]),
        path=pkgpath,
    )

    return (pkg,)


def install(repo: Repository, package: Package, *, quiet: bool = False) -> None:
    logger.info(
        "Adding package '%s' version '%s' to repository '%s'",
        package.name,
        package.version,
        repo.name,
    )

    from_path = package.path
    to_path = repo.path

    # {{{ copy files

    pkgfile = from_path.name
    shutil.copy2(from_path, to_path / pkgfile)

    if repo.key is not None:
        shutil.copy2(from_path.parent / f"{pkgfile}.sig", to_path / f"{pkgfile}.sig")

    # }}}

    # {{{ add to repo

    cargs: tuple[Any, ...] = ()
    if quiet:
        cargs = (*cargs, "--quiet")

    if repo.key is not None:
        cargs = (*cargs, "--verify", "--sign", "--key", repo.key)

    cargs = (*cargs, "--remove", "--prevent-downgrade", repo.db, pkgfile)

    call("repo-add", cargs, quiet=quiet, cwd=to_path)

    # }}}


def check(package: Package, *, quiet: bool = True) -> int:
    logger.info("Checking package '%s' version '%s'", package.name, package.version)

    if quiet:
        cargs: tuple[Any, ...] = (package.path,)
    else:
        cargs = ("-i", package.path)

    # NOTE: adding PYTHONWARNINGS to remove some noise from namcap output
    return call("namcap", cargs, quiet=quiet, environ={"PYTHONWARNINGS": "ignore"})


# }}}


def main(
    repo: Repository,
    pkgbuild: pathlib.Path,
    *,
    repackage: bool = True,
    noextract: bool = True,
    keep: bool = False,
    quiet: bool = True,
) -> int:
    if not pkgbuild.exists():
        logger.error("path does not exist: '%s'", pkgbuild)
        return 1

    pkgbuild = pkgbuild.resolve()
    if pkgbuild.is_dir():
        pkgbuild = pkgbuild / "PKGBUILD"

    if not pkgbuild.is_file():
        logger.error("path is not a (PKGBUILD) file: '%s'", pkgbuild)
        return 1

    packages = make(
        repo,
        pkgbuild,
        repackage=repackage,
        noextract=noextract,
        quiet=quiet,
    )

    if not packages:
        logger.error("no packages were built")
        return 1

    for pkg in packages:
        install(repo, pkg, quiet=quiet)
        if repo.namcap:
            check(pkg, quiet=False)

    if not keep:
        builddir = packages[0].path.parent
        shutil.rmtree(builddir)

    return 0


if __name__ == "__main__":
    import argparse

    import shtab

    class HelpFormatter(
        argparse.ArgumentDefaultsHelpFormatter,
        argparse.RawDescriptionHelpFormatter,
    ):
        pass

    choices = get_from_config(SCRIPT_CONFIG_FILE)
    if not choices:
        logger.error("No valid repositories configured in '%s'", SCRIPT_CONFIG_FILE)
        raise SystemExit(1)

    parser = argparse.ArgumentParser(
        allow_abbrev=False,
        formatter_class=HelpFormatter,
        description=SCRIPT_LONG_HELP,
    )
    shtab.add_argument_to(parser, ["--print-completion"])
    parser.add_argument(  # type: ignore[attr-defined]
        "pkgbuild",
        type=pathlib.Path,
        nargs="?",
        default="PKGBUILD",
        help="path to PKGBUILD build script",
    ).complete = shtab.FILE
    parser.add_argument(
        "-r",
        "--repo",
        choices=list(choices),
        default=next(iter(choices.values())).name,
        help="path to local repository",
    )
    parser.add_argument(
        "-R",
        "--repackage",
        action="store_true",
        help="package contents without rebuilding",
    )
    parser.add_argument(
        "--keep",
        action="store_true",
        help="keep all build artifacts",
    )
    parser.add_argument(
        "-e",
        "--noextract",
        action="store_true",
        help="do not extract source files (use existing $srcdir)",
    )
    parser.add_argument(
        "-q",
        "--quiet",
        action="store_true",
        help="suppress script output",
    )
    parser.add_argument(
        "--suppress-output",
        action="store_true",
        help="suppress output from makepkg and friends",
    )
    args = parser.parse_args()

    if not args.quiet:
        logger.setLevel(logging.INFO)

    raise SystemExit(
        main(
            choices[args.repo],
            args.pkgbuild,
            repackage=args.repackage,
            noextract=args.noextract,
            keep=args.keep,
            quiet=args.suppress_output,
        )
    )
