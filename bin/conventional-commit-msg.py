#!/usr/bin/env python

# SPDX-FileCopyrightText: 2023 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from __future__ import annotations

import logging
import pathlib
import re
from dataclasses import dataclass, replace

import platformdirs
import rich.logging

# {{{ script

logger = logging.getLogger(pathlib.Path(__file__).stem)
logger.setLevel(logging.ERROR)
logger.addHandler(rich.logging.RichHandler())

SCRIPT_PATH = pathlib.Path(__file__)
SCRIPT_CONFIG_DIR = platformdirs.user_config_path("yorick.scripts.com")
SCRIPT_CONFIG_FILE = SCRIPT_CONFIG_DIR / "conventional-commit-msg.toml"
SCRIPT_LONG_HELP = f"""\
A script that checks the Conventional Commit formatting style

    https://www.conventionalcommits.org/

that briefly looks like

    <type>([optional scope]): <description>

    [optional body]

    [optional footer(s)]

The supported types can be added to the configuration file

    {SCRIPT_CONFIG_FILE}

as

    [config]
    enabled = true
    line_length = 78
    types = build, breaking

The script can be used as a commit hook in git by creating a
file called '.git/hooks/commit-msg' with the contents

    #!/usr/bin/sh

    python {SCRIPT_PATH.name} $@

Examples:

    > {SCRIPT_PATH.name} --install-hook default
    > {SCRIPT_PATH.name} "feat(jit): add specialization for small integers"
"""

# }}}


# {{{ configuration

CC_EXAMPLE_MESSAGE = """\
Conventional commit example formatting

    <type>([optional scope]): <description>

    [optional body]

    [optional footer(s)]
"""

CC_COMMIT_HOOK = f"""\
#!/usr/bin/sh

# NOTE: checks conventional commit formatting
{SCRIPT_PATH.name} --quiet --project {{project}} $@
"""

CC_HEADER_RE = re.compile(r"(?P<mtype>\w+)(\([\w\-_]+\))?!?: ([\w\- ]+)")
CC_FOOTER_RE = re.compile(r"(?P<token>[\w\-]+|BREAKING CHANGE)(: | #)([\w\- ]+)")

#: https://git-scm.com/docs/git-rebase
CC_AUTOSQUASH_TYPES = frozenset(["squash", "fixup", "amend"])
CC_DEFAULT_TYPES = frozenset(["chore", "feat", "fix", "style", "test"])
CC_DEFAULT_LINE_LENGTH = 52


@dataclass(frozen=True)
class Config:
    #: If *True*, check the commit message against the conventional commit format.
    enabled: bool = False

    #: Maximum line length.
    line_length: int = CC_DEFAULT_LINE_LENGTH

    #: Types for the conventional commits.
    types: frozenset[str] = CC_DEFAULT_TYPES


def get_from_config(path: pathlib.Path) -> dict[str, Config]:
    config = Config()
    if not path.exists():
        logger.error("Configuration file '%s' does not exist", path)
        return {"default": config}

    import tomllib

    with open(path, "rb") as f:
        settings = tomllib.load(f)

    values = settings.pop("default", None)
    if values is not None:
        values["types"] = CC_DEFAULT_TYPES | frozenset(values.get("types", []))
        config = replace(config, **values)

    configs = {"default": config}
    for cid, values in settings.items():
        values["types"] = CC_DEFAULT_TYPES | frozenset(values.get("types", []))
        configs[cid] = replace(config, **values)

    return configs


def install_hook(project: str | None = None) -> int:
    gitdir = pathlib.Path().cwd() / ".git"
    if not gitdir.exists():
        logger.error("Not in a git repository.")
        return 1

    hookfile = gitdir / "hooks" / "commit-msg"
    if hookfile.exists():
        logger.error("Hook for 'commit-msg' already exists: '%s'.", hookfile)
        return 1

    if project is None:
        project = "default"

    with open(hookfile, "w", encoding="utf-8") as outf:
        outf.write(CC_COMMIT_HOOK.format(project=project))

    import stat

    hookfile.chmod(stat.S_IRWXU | stat.S_IRGRP | stat.S_IXGRP | stat.S_IROTH)

    return 0


# }}}


# {{{ parsing


def parse_commit_message(msg: str) -> list[str]:
    from itertools import takewhile

    # NOTE: we only look at lines until the first comment
    lines = list(takewhile(lambda line: not line.startswith("#"), msg.split("\n")))

    return lines


# }}}


# {{{ checking


def main(
    msg: str,
    *,
    line_length: int = CC_DEFAULT_LINE_LENGTH,
    types: frozenset[str] | None = None,
) -> int:
    if types is None:
        types = CC_DEFAULT_TYPES

    lines = parse_commit_message(msg)
    if not any(line for line in lines):
        logger.error("Empty commit message. %s", CC_EXAMPLE_MESSAGE)
        return 1

    header = lines[0]
    if any(header.startswith(f"{name}!") for name in CC_AUTOSQUASH_TYPES):
        # NOTE: if this is an autosquash message, we leave it alone
        return 0

    if any(len(line) > line_length for line in lines):
        logger.error("Lines are too long (max length %d).", line_length)
        for i, line in enumerate(lines):
            if len(line) > line_length:
                logger.error("  %d: %s", i, line)

        return 1

    match = CC_HEADER_RE.match(header)

    if not match:
        logger.error(
            "Invalid commit message header: '%s'. %s",
            header,
            CC_EXAMPLE_MESSAGE,
        )
        return 1

    if match["mtype"] not in types:
        logger.error(
            "Invalid conventional commit type '%s'. Available types are '%s'.",
            match["mtype"],
            "', '".join(sorted(types)),
        )
        return 1

    if len(lines) > 1:  # noqa: SIM102
        if lines[1]:
            logger.error("First line after description is not a blank line.")
            return 1

        # TODO: check trailers?

    return 0


# }}}


if __name__ == "__main__":
    import argparse

    import shtab

    class HelpFormatter(
        argparse.ArgumentDefaultsHelpFormatter,
        argparse.RawDescriptionHelpFormatter,
    ):
        pass

    choices = get_from_config(SCRIPT_CONFIG_FILE)
    if not choices:
        logger.error("No projects configured in '%s'.", SCRIPT_CONFIG_FILE)
        raise SystemExit(1)

    parser = argparse.ArgumentParser(
        formatter_class=HelpFormatter,
        description=SCRIPT_LONG_HELP,
    )
    shtab.add_argument_to(parser, ["--print-completion"])
    parser.add_argument(
        "message",
        nargs="?",
        help="Commit message or filename to check",
    )
    parser.add_argument(
        "-p",
        "--project",
        choices=list(choices),
        default="default",
        help="Section name to use for settings",
    )
    parser.add_argument(
        "-q",
        "--quiet",
        action="store_true",
        help="only show error messages",
    )
    parser.add_argument(
        "--install-hook",
        default=None,
        help="installs a git hook for 'commit-msg'",
    )
    args = parser.parse_args()

    if not args.quiet:
        logger.setLevel(logging.INFO)

    if args.install_hook is not None:
        raise SystemExit(install_hook(args.install_hook))
    elif args.message is None:
        logger.error("No message provided.")
        raise SystemExit(1)

    c = choices[args.project]
    if not c.enabled:
        raise SystemExit(0)
    logger.info("Loaded config '%s'.", SCRIPT_CONFIG_FILE)

    path = pathlib.Path(args.message)
    if path.exists():
        args.message = path.read_text(encoding="utf-8")

    raise SystemExit(main(args.message, line_length=c.line_length, types=c.types))
