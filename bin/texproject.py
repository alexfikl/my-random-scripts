#!/usr/bin/env python

# SPDX-FileCopyrightText: 2023 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from __future__ import annotations

import logging
import pathlib
import shutil

import platformdirs
import rich.logging

# {{{ script

log = logging.getLogger(pathlib.Path(__file__).stem)
log.setLevel(logging.ERROR)
log.addHandler(rich.logging.RichHandler())

SCRIPT_PATH = pathlib.Path(__file__)
SCRIPT_CONFIG_DIR = platformdirs.user_config_path("yorick.scripts.com")
SCRIPT_CONFIG_TEMPLATE_DIR = SCRIPT_CONFIG_DIR / "texproject"
SCRIPT_LONG_HELP = f"""\
A script to quickly initialize some TeX projects.

Templates are located at

    {SCRIPT_CONFIG_TEMPLATE_DIR}

and are named like `<TEMPLATE_NAME>.ext`, where `tex` and `typ` formats are
supported. The format is mainly used to generate a Makefile to build the project.

Example:

    > {SCRIPT_PATH.name} -t report 2023-12-my-report
"""

# }}}


# {{{ utils

SUPPORTED_EXTENSIONS = (".tex", ".typ")

# {{{ makefiles

VALE_INIT_TEMPLATE = """\
StylesPath = styles
MinAlertLevel = suggestion
Packages = Google

[*]
BasedOnStyles = Vale, Google
"""

TEX_JUSTFILE_TEMPLATE = """\
TEXMK := 'latexmk'
OUTDIR := 'latex.out'
TEXFLAGS := '-pdflua'
TEXFILE := '%(name)s'

_default:
    @just --list

[doc("Build the document PDF")]
pdf:
    {{ TEXMK }} {{ TEXFLAGS }} -output-directory={{ OUTDIR }} {{ TEXFILE }}.tex
    @cp {{ OUTDIR }}/{{ TEXFILE }}.pdf .

[doc("Remove temporary build files")]
clean:
    rm -rf {{ OUTDIR }}

[doc("Remove all generated files")]
purge: clean
    rm -rf {{ TEXFILE }}.pdf

[doc("Run chktex and lacheck over the document .tex file")]
lint:
    lacheck {{ TEXFILE }}

[doc("Open the generated PDF file in your favourite viewer")]
view app="xdg-open":
    @{{ app }} {{ TEXFILE }}.pdf
"""

TYPST_JUSTFILE_TEMPLATE = """\
TYPST := 'typst'
TYPSTFLAGS := ''
TYPSTFILE := '%(name)s'

_default:
    @just --list

[doc("Build the document PDF")]
pdf:
    {{ TYPST }} compile {{ TYPSTFLAGS }} {{ TYPSTFILE }}.typ

[doc("Remove temporary build files")]
clean:
    @echo 'Nothing to remove'

[doc("Remove all generated files")]
purge:
    rm -rf {{ TYPSTFILE }}.pdf

[doc("Open the generated PDF file in your favourite viewer")]
view app="xdg-open":
    @{{ app }} {{ TYPSTFILE }}.pdf
"""

# }}}


def get_template_choices() -> dict[str, str]:
    if not SCRIPT_CONFIG_TEMPLATE_DIR.exists():
        SCRIPT_CONFIG_TEMPLATE_DIR.mkdir()

    return {
        f.stem: f.suffix
        for f in SCRIPT_CONFIG_TEMPLATE_DIR.iterdir()
        if f.name[0] != "."
    }


# }}}


# {{{ make_project


def make_project(
    name: str,
    template: str,
    ext: str,
    *,
    outfile: str | None = None,
    single: bool = False,
    force: bool = False,
) -> int:
    if not SCRIPT_CONFIG_TEMPLATE_DIR.exists():
        SCRIPT_CONFIG_TEMPLATE_DIR.mkdir()

    filename = (SCRIPT_CONFIG_TEMPLATE_DIR / template).with_suffix(ext)
    if not filename.exists():
        log.error(
            "Template '%s' does not exist in '%s'", template, SCRIPT_CONFIG_TEMPLATE_DIR
        )
        return 1

    if single:
        dirname = pathlib.Path.cwd()
    else:
        dirname = pathlib.Path.cwd() / name
        if not force and dirname.exists():
            log.error("Project directory '%s' already exists", dirname)
            return 1

        if not dirname.exists():
            dirname.mkdir()

    if outfile is None:
        dstfile = (dirname / name).with_suffix(ext)
    else:
        dstfile = dirname / outfile

        if dstfile.suffix != ext:
            log.warning("Output file is not a '%s' file: '%s'", ext, dstfile)
            dstfile = dstfile.with_suffix(ext)

    if single and not force and dstfile.exists():
        log.error("Project file '%s' already exists.", dstfile)
        return 1

    log.info("Making project from '%s' template at: '%s'", name, dirname)
    shutil.copyfile(filename, dstfile)

    if single:
        log.info(
            "Add the following to your 'justfile':\n\ndoc-%s:\n    @just pdf %s",
            dstfile.stem,
            dstfile.stem,
        )
        return 0

    if ext in SUPPORTED_EXTENSIONS:
        makefile = dirname / "justfile"
        with open(makefile, "w", encoding="utf-8") as f:
            if ext == ".tex":
                f.write((TEX_JUSTFILE_TEMPLATE % {"name": dstfile.stem}).strip())
            elif ext == ".typ":
                f.write((TYPST_JUSTFILE_TEMPLATE % {"name": dstfile.stem}).strip())
            else:
                raise AssertionError(ext)

    return 0


# }}}


if __name__ == "__main__":
    import argparse

    import shtab

    class HelpFormatter(
        argparse.ArgumentDefaultsHelpFormatter,
        argparse.RawDescriptionHelpFormatter,
    ):
        pass

    template_to_ext = get_template_choices()
    choices = list(template_to_ext)
    if not choices:
        log.error("No templates in '%s'", SCRIPT_CONFIG_TEMPLATE_DIR)
        raise SystemExit(1)

    parser = argparse.ArgumentParser(
        formatter_class=HelpFormatter,
        description=SCRIPT_LONG_HELP,
    )
    shtab.add_argument_to(parser, ["--print-completion"])
    parser.add_argument(  # type: ignore[attr-defined]
        "name", help="a directory name for the project"
    ).complete = shtab.DIRECTORY
    parser.add_argument(
        "-t",
        "--template",
        choices=choices,
        default=choices[0],
        help="the name of the template used to initialize the project",
    )
    parser.add_argument(
        "--no-directory",
        action="store_true",
        help="If true, only the template will be copied to the current directory",
    )
    parser.add_argument(
        "-o",
        "--output",
        default=None,
        help="name of the main file generated from the template",
    )
    parser.add_argument(
        "-f",
        "--force",
        action="store_true",
        help="Overwrite files if already present",
    )
    parser.add_argument(
        "-q",
        "--quiet",
        action="store_true",
        help="only show error messages",
    )
    args = parser.parse_args()

    if not args.quiet:
        log.setLevel(logging.INFO)

    raise SystemExit(
        make_project(
            args.name,
            args.template,
            template_to_ext[args.template],
            outfile=args.output,
            single=args.no_directory,
            force=args.force,
        )
    )
