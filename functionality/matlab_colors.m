% SPDX-FileCopyrightText: 2025 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

clear all;
close all;

palettes = ["gem", "gem12", "glow", "glow12", "sail", "reef", "meadow", "dye", "earth"];

for name = palettes
    filename = sprintf("%s.mplstyle", name);
    f = fopen(filename, 'w');

    fprintf(f, '# Color cycle "%s" obtained from MATLAB %s\n', name, version);
    fprintf(f, '# This file has been automatically generated. Do not modify!\n\n');

    colors = orderedcolors(name);
    colornames = string.empty([length(colors), 0]);
    for i = 1:length(colors)
        r = dec2hex(round(colors(i, 1) * 255));
        g = dec2hex(round(colors(i, 2) * 255));
        b = dec2hex(round(colors(i, 3) * 255));
        colornames(i) = string(lower(sprintf('#%02s%02s%02s', r, g, b)));
    end

    fprintf(f, 'axes.prop_cycle: cycler(''color'', [''%s''])', strjoin(colornames, "', '"));
    fclose(f);
end
