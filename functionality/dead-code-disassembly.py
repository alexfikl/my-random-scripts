# SPDX-FileCopyrightText: 2021 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
Check that a dead branch actually gets removed in the bytecode.
"""

from __future__ import annotations


def func(x):
    assert x is not None

    if False:  # pylint: disable=using-constant-test
        x = x / 2

    return x


if __name__ == "__main__":
    import dis

    print(dis.dis(func))
