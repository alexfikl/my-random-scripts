# SPDX-FileCopyrightText: 2021 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

r"""
This script shows off interpolation using the Fast Fourier Transform.

Basically, we take :math:`sin^2 \theta_i` with :math:`n` equidistnt nodes and
interpolate that to a grid with :math:`m` equidistant nodes.

This should give the same result as just evaluating at the :math:`m` nodes.
"""

from __future__ import annotations

import numpy as np
import numpy.linalg as la


def interpolate(x: np.ndarray, m: int) -> np.ndarray:
    return np.fft.irfft(np.fft.rfft(x), m) * m / x.size


def run_different_sizes(n: int = 48, m: int = 64) -> None:
    def sine_sqr(npoints: int) -> np.ndarray:
        theta = np.linspace(0.0, 2.0 * np.pi, npoints, endpoint=False)
        return theta, np.sin(theta) ** 2

    theta_n, x = sine_sqr(n)
    theta_m, y = sine_sqr(m)
    x_interp = interpolate(x, m)

    error = la.norm(y - x_interp) / la.norm(y)
    print(f"error: {error:.5e}")

    from libscratch import figure

    with figure("fft-interpolation-result") as fig:
        ax = fig.gca()
        ax.plot(theta_n, x, label="$x$")
        ax.plot(theta_m, y, label="$y$")
        ax.plot(theta_m, x_interp, label=r"$\hat{x}$")  # noqa: RUF027
        ax.set_xlabel(r"$\theta$")
        ax.legend()


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("-n", type=int, default=48)
    parser.add_argument("-m", type=int, default=64)

    args = parser.parse_args()
    run_different_sizes(n=args.n, m=args.m)
