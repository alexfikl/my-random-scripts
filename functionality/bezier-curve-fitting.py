# SPDX-FileCopyrightText: 2021 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from __future__ import annotations

import numpy as np
import numpy.linalg as la
import scipy.linalg as sla


def bernstein_poly(n: int, t: np.ndarray) -> np.ndarray:
    from math import comb

    return np.stack([comb(n, k) * t**k * (1 - t) ** (n - k) for k in range(n + 1)])


def bernstein_poly_der(n: int, t: np.ndarray) -> np.ndarray:
    def db(k):
        if k == 0:
            return -n * (1 - t) ** (n - 1)
        elif k == n:
            return n * t ** (n - 1)
        else:
            return k * t ** (k - 1) * (1 - t) ** (n - k) - (n - k) * t**k * (1 - t) ** (
                n - k - 1
            )

    from math import comb

    return np.stack([comb(n, k) * db(k) for k in range(n + 1)])


def solid_body_rotation_position(x0, t, *, omega, height):
    return np.stack([
        np.cos(omega * t) * x0[0] - np.sin(omega * t) * x0[1] - x0[0],
        np.sin(omega * t) * x0[0] + np.cos(omega * t) * x0[1] - x0[1],
        height * t + x0[2],
    ])


def solid_body_rotation_velocity(x0, t, *, omega, height):
    return np.stack([
        omega * (-np.sin(omega * t) * x0[0] - np.cos(omega * t) * x0[1]),
        omega * (np.cos(omega * t) * x0[0] - np.sin(omega * t) * x0[1]),
        np.full_like(t, height),
    ])


def bezier_curve_fit_lstsq(omega, height, *, n, npoints):
    t = np.linspace(0.0, 1.0, npoints)
    x0 = np.array([1, 0, 0])

    # {{{ fitting

    mat = bernstein_poly(n, t)
    b = solid_body_rotation_position(x0, t, omega=omega, height=height)

    p, _, _, _ = sla.lstsq(mat.T, b.T)
    p[0, :] = 0.0
    x = (mat.T @ p).T
    print("residual", la.norm(x - b) / la.norm(b))

    # }}}

    return t, mat, b, x


def bezier_curve_fit_opt(omega, height, *, n, npoints):
    ambient_dim = 3

    def fun(p, *, mpoly):
        p = np.vstack([np.zeros(ambient_dim), p.reshape(-1, ambient_dim)])
        return (mpoly.T @ p).T

    def fun_lsq(p, *, mpoly, mb):
        return fun(p, mpoly=mpoly) - mb

    def jac(p, *, mpoly, mb):
        # FIXME: more vectorized?
        df = np.empty((mpoly.shape[1] * ambient_dim, p.size))
        c = np.zeros((ambient_dim, mpoly.shape[1]))

        for i, (j, k) in enumerate(np.ndindex(mpoly.shape[0] - 1, ambient_dim)):
            c[k, :] = mpoly[j + 1, :]
            df[:, i] = c.flat
            c[:, :] = 0.0

        return df

    t = np.linspace(0.0, 1.0, npoints)
    x0 = np.array([1, 0, 0])

    # {{{ fitting

    mat = bernstein_poly(n, t)
    b = solid_body_rotation_position(x0, t, omega=omega, height=height)

    import scipy.optimize

    p0 = np.zeros((mat.shape[0] - 1, x0.size))
    r = scipy.optimize.least_squares(
        lambda x: fun_lsq(x, mpoly=mat, mb=b).reshape(-1),
        p0.reshape(-1),
        jac=lambda x: jac(x, mpoly=mat, mb=b),
        ftol=5.0e-16,
        xtol=5.0e-16,
        gtol=5.0e-16,
        verbose=2,
    )

    x = fun(r.x, mpoly=mat)
    print("residual", la.norm(x - b) / la.norm(b), la.norm(jac(r.x, mpoly=mat, mb=b)))

    # }}}

    return t, mat, b, x


def bezier_curve_fit(omega: float, height: float, n: int = 8, npoints: int = 256):
    # t, mat, b, x = bezier_curve_fit_lstsq(omega, height, n=n, npoints=npoints)
    t, mat, b, x = bezier_curve_fit_opt(omega, height, n=n, npoints=npoints)

    import matplotlib.pyplot as mp
    import matplotx

    with mp.style.context(matplotx.styles.dufte):
        fig = mp.figure(figsize=(10, 10), dpi=300)

        # {{{ fitting

        ax = fig.gca()

        (line,) = ax.plot(t, x[0], label="$x$")
        ax.plot(t, b[0], ls="--", color=line.get_color())
        (line,) = ax.plot(t, x[1], label="$y$")
        ax.plot(t, b[1], ls="--", color=line.get_color())
        (line,) = ax.plot(t, x[2], label="$z$")
        ax.plot(t, b[2], ls="--", color=line.get_color())

        ax.set_xlabel("$t$")
        ax.legend()

        fig.savefig("bezier-curve-fitting")
        fig.clf()

        # }}}

        # {{{ polynomials

        ax = fig.gca()
        for k in range(mat.shape[0]):
            ax.plot(t, mat[k, :], label=rf"$\phi_{k}^{n}$")

        ax.set_xlabel("$t$")
        ax.legend()

        fig.savefig("bezier-curve-bernstein-poly")
        fig.clf()

        # }}}


if __name__ == "__main__":
    bezier_curve_fit(4.0 * np.pi, 3.0)
