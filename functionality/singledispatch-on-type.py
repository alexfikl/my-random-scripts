# SPDX-FileCopyrightText: 2021 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from __future__ import annotations

from functools import singledispatch
from numbers import Number
from typing import Any

import numpy as np


@singledispatch
def dump(obj: Any):
    print("obj is 'Any': ", obj)
    raise NotImplementedError


@dump.register(type)
def _(obj: type):
    print("obj is 'type': ", obj)


@dump.register(object)
def _(obj: object):
    print("obj is 'object': ", obj)


@dump.register(bool)
@dump.register(Number)
def _(obj: Number):
    print(f"obj is 'Number' ({type(obj)}): {obj}")


if __name__ == "__main__":
    x = {"x": 1}

    dump(int)
    dump(1)
    dump(1.0)
    dump(np.float32(1.0))
    dump(True)  # noqa: FBT003
    dump(x)
    dump(type(x))
