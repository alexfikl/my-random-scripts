# SPDX-FileCopyrightText: 2021 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
Play around with :mod:`dufte` a bit.
"""

from __future__ import annotations

import dufte
import numpy as np

if __name__ == "__main__":
    from libscratch import figure

    np.random.seed(0)
    rng = np.random.default_rng()

    with figure("dufte-example") as fig:
        ax = fig.gca()

        x0 = np.linspace(0.0, 3.0, 100)
        y0 = x0 / (x0 + 1)
        y0 += 0.1 * rng.random(len(y0))
        ax.plot(x0, y0, label="no balancing")

        x1 = np.linspace(0.0, 3.0, 100)
        y1 = 1.5 * x1 / (x1 + 1)
        y1 += 0.1 * rng.random(len(y1))
        ax.plot(x1, y1, label="CRV-27")

        x2 = np.linspace(0.0, 3.0, 100)
        y2 = 1.6 * x2 / (x2 + 1)
        y2 += 0.1 * rng.random(len(y2))
        ax.plot(x2, y2, label="CRV-27*")
        ax.set_xlabel("$x$")
        ax.set_ylabel("$y$")

        dufte.legend(ax)
