# SPDX-FileCopyrightText: 2021 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from __future__ import annotations

import pathlib
from contextlib import contextmanager
from typing import Any

import matplotlib.pyplot as plt

# {{{ plotting


@contextmanager
def figure(filename: str, **kwargs: Any) -> plt.Figure:
    fig = plt.figure(**kwargs)

    try:
        yield fig
    finally:
        ext = plt.rcParams["savefig.format"]
        filename = pathlib.Path(filename).with_suffix(f".{ext}")
        print(f"saving figure to '{filename}'")

        fig.savefig(filename)
        plt.close(fig)


def _set_matplotlib_defaults() -> None:
    import matplotlib
    import matplotlib.pyplot as mp

    try:
        import dufte

        mp.style.use(dufte.style)
    except ImportError:
        print("'dufte' package not found")

    # FIXME: this does not seem to be a documented way to check for tex support,
    # but it works for our current usecase. it also does not catch the actual
    # issue on porter, which is the missing `cm-super` package
    usetex = matplotlib.checkdep_usetex(s=True)

    mp.rc("figure", figsize=(10, 10), dpi=300)
    mp.rc("figure.constrained_layout", use=True)
    mp.rc("text", usetex=usetex)
    mp.rc("legend", fontsize=32)
    mp.rc("lines", linewidth=2.5, markersize=10)
    mp.rc("axes", labelsize=32, titlesize=32)
    mp.rc("xtick", labelsize=24)
    mp.rc("ytick", labelsize=24)


_set_matplotlib_defaults()

# }}}
