# SPDX-FileCopyrightText: 2021 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from __future__ import annotations

import matplotlib.pyplot as mp
import numpy as np
import numpy.linalg as la


def jacobi(
    mat: np.ndarray,
    b: np.ndarray,
    *,
    x0: np.ndarray | None = None,
    maxit: int | None = None,
    rtol: float | None = None,
    callback=None,
) -> np.ndarray:
    if x0 is None:
        x0 = np.zeros_like(b)

    if maxit is None:
        maxit = b.size

    if rtol is None:
        rtol = 1.0e-8

    rtol = rtol * la.norm(b, ord=2)

    x = x0
    for n in range(maxit):
        x0 = np.zeros_like(x)

        for i in range(mat.shape[0]):
            s1 = mat[i, :i] @ x[:i]
            s2 = mat[i, i + 1 :] @ x[i + 1 :]

            x0[i] = (b[i] - s1 - s2) / mat[i, i]

        if la.norm(x - x0) < rtol:
            break

        x = x0
        if callback is not None:
            callback(n, mat @ x - b)

        residual = la.norm(mat @ x - b) / la.norm(b)
        print(f"[{n:08d}] residual {residual:.5e}")

    return x


def main(n: int = 64, mode: str = "heat"):
    if mode == "random":
        rng = np.random.default_rng()
        mat = rng.random((n, n))
        b = rng.random(n)

        # ensure the matrix is diagonally dominant for Jacobi to work
        mat = mat + np.diag(3 + np.sum(mat, axis=0))
    elif mode == "heat":
        d = np.ones(n)
        # sort of a (-Delta + k) operator
        mat = (
            np.diag(d, k=-1)[:-1, :-1] - 3 * np.diag(d, k=0) + np.diag(d, k=1)[:-1, :-1]
        )
        b = np.sin(np.linspace(0, 2.0 * np.pi, n))
    else:
        raise ValueError(f"unsupported mode: '{mode}'")

    def callback(it: int, residual: np.ndarray) -> None:
        fig = mp.figure(figsize=(10, 10), dpi=300)
        ax = fig.gca()

        ax.plot(residual)
        ax.set_ylabel("$A x - b$")
        ax.set_ylim([-0.5, 0.5])
        fig.savefig(f"jacobi_residual_{it:08d}")
        mp.close(fig)

    x = jacobi(mat, b, callback=callback)
    print(la.norm(mat @ x - b) / la.norm(b))


if __name__ == "__main__":
    main()
