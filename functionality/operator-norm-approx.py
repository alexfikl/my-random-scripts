# SPDX-FileCopyrightText: 2021 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from __future__ import annotations

import numpy as np
import numpy.linalg as la


def main(n: int = 512, p: float = 2, a: float = -10.0, b: float = 10.0) -> None:
    rng = np.random.default_rng(seed=42)

    A = a + (b - a) * rng.random(size=(n, n))
    A_norm = la.norm(A, ord=p)

    A_norm_vec = 0
    for _ in range(16):
        x = rng.normal(size=n)
        A_norm_vec = max(A_norm_vec, la.norm(A @ x, ord=p) / la.norm(x, ord=p))

    print(f"matrix norm {A_norm:.12e} vector norm {A_norm_vec:.12e}")


if __name__ == "__main__":
    main()
