# SPDX-FileCopyrightText: 2021 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
Based on `https://github.com/numpy/numpy/issues/18471#issuecomment-785019387`__.
"""

from __future__ import annotations

import ctypes
import os
import sys

import numpy as np


def is_ilp64():
    """Copied from `https://github.com/numpy/numpy/blob/main/tools/openblas_support.py`__"""
    if os.environ.get("NPY_USE_BLAS_ILP64", "0") == "0":
        return False

    is_32_bit = sys.maxsize < 2**32
    if is_32_bit:
        raise RuntimeError("'NPY_USE_BLAS_ILP64' set on 32-bit arch")

    return True


def openblas_get_config():
    # pylint: disable=protected-access
    dll = ctypes.CDLL(np._core._multiarray_umath.__file__)

    if is_ilp64():  # noqa: SIM108
        get_config = dll.openblas_get_config64_
    else:
        get_config = dll.openblas_get_config

    get_config.restype = ctypes.c_char_p
    return get_config().decode()


if __name__ == "__main__":
    res = openblas_get_config()
    print(f"OpenBLAS get_config\n{res}")
