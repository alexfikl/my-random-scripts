# SPDX-FileCopyrightText: 2021 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from __future__ import annotations

from dataclasses import dataclass

import numpy as np


@dataclass(frozen=True)
class MyClass:
    name: str

    @property
    def x(self):
        return np.zeros(42)

    @property
    def y(self):
        return np.zeros(32)


@dataclass(frozen=True)
class MyOtherClass(MyClass):
    x: np.ndarray
    y: np.ndarray


if __name__ == "__main__":
    ob2 = MyClass(name="ob2")
    ob1 = MyOtherClass(name="ob1", x=np.zeros(42), y=np.zeros(32))
