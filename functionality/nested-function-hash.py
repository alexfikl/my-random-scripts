# SPDX-FileCopyrightText: 2021 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from __future__ import annotations

from dataclasses import dataclass


@dataclass
class Person:
    name: str


def memoize_on_first_arg(function):
    def wrapper(obj, *args, **kwargs):
        key = (function,)

        if "locals" in getattr(function, "__qualname__", ""):
            print("nested function")

        if not hasattr(obj, "_memoize_dict"):
            obj._memoize_dict = {}

        if key not in obj._memoize_dict:
            obj._memoize_dict[key] = "cached"
            print(f"key not in dict: {key} hash {hash(key)}")
        else:
            print(f"key in dict: {key} hash {hash(key)}")

        return function(obj, *args, **kwargs)

    return wrapper


def get_name(obj):
    @memoize_on_first_arg
    def do_something(o, age):
        return f"name {o.name} age {age}"

    return do_something(obj, 12)


if __name__ == "__main__":
    obj = Person(name="Alonso")

    for _ in range(5):
        get_name(obj)
