# SPDX-FileCopyrightText: 2021 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from __future__ import annotations

import numpy as np
import numpy.linalg as la


def main():
    import sympy as sp

    s_alpha, s_beta, s_gamma = sp.symbols("alpha beta gamma", real=True)
    s_rot = sp.rot_axis3(s_alpha) @ sp.rot_axis2(s_beta) @ sp.rot_axis1(s_gamma)
    sym_rot = sp.lambdify([s_alpha, s_beta, s_gamma], s_rot)

    def check(alpha, beta, gamma):
        from scipy.spatial.transform import Rotation

        scipy_rot = Rotation.from_euler("ZYX", (-alpha, -beta, -gamma)).as_matrix()
        sympy_rot = sym_rot(alpha, beta, gamma)

        print(scipy_rot)
        print(sympy_rot)
        print(la.norm(scipy_rot - sympy_rot) / la.norm(sympy_rot))

    rng = np.random.default_rng()
    check(0, 0, rng.uniform(0.1, 2 * np.pi - 0.1))
    check(0, rng.uniform(0.1, 2 * np.pi - 0.1), 0)
    check(rng.uniform(0.1, 2 * np.pi - 0.1), 0, 0)

    for _ in range(16):
        check(
            rng.uniform(0.1, 2 * np.pi - 0.1),
            rng.uniform(0.1, 2 * np.pi - 0.1),
            rng.uniform(0.1, 2 * np.pi - 0.1),
        )


if __name__ == "__main__":
    main()
