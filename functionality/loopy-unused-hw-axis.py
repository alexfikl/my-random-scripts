# SPDX-FileCopyrightText: 2021 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from __future__ import annotations

import numpy as np

try:
    import loopy as lp
except ImportError as exc:
    raise RuntimeError("script requires 'loopy'") from exc


def run_loopy_kernel(queue, n=16, m=5):
    knl = lp.make_kernel(
        "{[i, j]: 0 <= i < n and 1 <= j < m - 1}",
        """
            result[i, 0] = ary[i, 0]
            result[i, m - 1] = ary[i, 1]
            result[i, j] = j
            """,
    )
    knl = lp.tag_array_axes(knl, "ary", "stride:auto,stride:auto")
    knl = lp.split_iname(knl, "j", 16, inner_tag="l.0")
    knl = lp.tag_inames(knl, {"i": "g.0"})
    knl = lp.add_inames_for_unused_hw_axes(knl)

    import pyopencl.array as cl_array

    ary = cl_array.empty(queue, (2, n), dtype=np.int64)
    ary[0, :] = -42
    ary[1, :] = +42

    _, (result,) = knl(queue, ary=ary.T, n=n, m=m)
    result = result.get()
    print(result)


if __name__ == "__main__":
    import pyopencl as cl

    ctx = cl.create_some_context()
    queue = cl.CommandQueue(ctx)

    run_loopy_kernel(queue)
