# SPDX-FileCopyrightText: 2021 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from __future__ import annotations

from configparser import ConfigParser

CONF_SECTION = "pudb"


def main(filename: str):
    conf = ConfigParser(allow_no_value=True)
    conf.add_section(CONF_SECTION)

    conf.set(CONF_SECTION, "custom_shell", None)
    conf.set(CONF_SECTION, "custom_theme", None)
    conf.set(CONF_SECTION, "custom_stringifier", None)
    conf.set(CONF_SECTION, "theme", "monokai")

    with open(filename, "w", encoding="utf-8") as outf:
        conf.write(outf, space_around_delimiters=False)


if __name__ == "__main__":
    main("example.conf")
