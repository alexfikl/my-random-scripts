# SPDX-FileCopyrightText: 2021 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from __future__ import annotations

import logging

import colorama


class ColoramaFormatter(logging.Formatter):
    def format(self, record: logging.LogRecord) -> str:
        record.msg = record.msg.format(c=colorama)
        return super().format(record)


log_format = (
    colorama.Fore.YELLOW
    + "%(levelname)s"
    + ":"
    + colorama.Fore.GREEN
    + "%(name)s"
    + colorama.Style.RESET_ALL
    + ": "
    + "%(message)s"
)
handler = logging.StreamHandler()
handler.setFormatter(ColoramaFormatter(log_format))
logging.basicConfig(level=logging.INFO, handlers=[handler])

logging.info("{c.Fore.GREEN}OMGx%d {c.Style.RESET_ALL}", 10)
