% SPDX-FileCopyrightText: 2017-2023 The Simons Foundation, Inc.
% SPDX-FileCopyrightText: 2021 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: Apache-2.0

function nufft_poisson_solve()
    % https://finufft.readthedocs.io/en/latest/tutorial/peripois2d.html

    % width of bumps
    w0 = 0.1;
    % right-hand side source term
    src = @(x, y) ...
        exp(-0.5 * ((x - 1).^2 + (y-2).^2) / w0^2) ...
        - exp(-0.5 * ((x - 3).^2 + (y - 5).^2) / w0^2);

    % nonlinear domain mapping
    map = @(t, s) [...
        t + 0.5 * sin(t) + 0.2 * sin(2 * s); ...
        s + 0.3 * sin(2 * s) + 0.3 * sin(s - t)];
    % its 2x2 Jacobian
    mapJ = @(t, s) [...
        1 + 0.5 * cos(t), 0.4 * cos(2 * s); ...
        -0.3 * cos(s - t), 1 + 0.6 * cos(2 * s) + 0.3 * cos(s - t)];

    % NUFFT precision
    tol = 1.0e-12;
    % convergence study of grid points per side
    ns = 80:40:240;
    uval = zeros(size(ns));

    for i = 1:numel(ns), n = ns(i);
        n
        % uniform grid
        t = 2 * pi * (0:n - 1)/n;
        [tt, ss] = ndgrid(t, t);
        t

        % non-uniform grid
        xxx = map(tt(:)', ss(:)');
        xx = reshape(xxx(1, :), [n, n]);
        yy = reshape(xxx(2, :), [n, n]);

        % jacobian
        J = mapJ(tt(:)', ss(:)');
        detJ = J(1, 1:n^2) .* J(2, n^2 + 1:end) - J(2, 1:n^2) .* J(1, n^2 + 1:end);

        % quadrature weights, including 1/(2pi)^2 in Euler-Fourier integral
        ww = detJ / n^2;
        % source term
        f = src(xx,yy);

        % modes to trust due to quadrature error
        Nk = 0.5 * n;
        Nk = 2 * ceil(Nk / 2);

        % use fft output mode ordering
        o.modeord = 1;
        % compute Euler-Fourier integral
        fhat = finufft2d1(xx(:), yy(:), f(:) .* ww(:), 1, tol, Nk, Nk, o);

        % Fourier mode grid
        k = [0:Nk / 2 - 1, -Nk / 2:-1];
        [kx, ky] = ndgrid(k, k);

        % inverse -Laplacian in k-space (as above)
        kfilter = 1./(kx.^2+ky.^2);
        kfilter(1, 1) = 0;
        kfilter(Nk / 2 + 1, :) = 0;
        kfilter(:, Nk / 2 + 1) = 0;

        % evaluate filtered F series at non-uniform points
        u = finufft2d2(xx(:), yy(:), -1, tol, kfilter .* fhat, o);
        u = reshape(real(u), [n, n]);

        % check convergence at same point
        uval(i) = u(1, 1);
        if i == 1
            fprintf('n = %4d Nk = %4d u(0, 0) = %.15e\n', n, Nk, u(1, 1));
        else
            fprintf('n = %4d Nk = %4d u(0, 0) = %.15e %.15e\n', ...
                n, Nk, u(1, 1), abs(uval(i) - uval(i - 1)));
        end
        zzz
    end
end
