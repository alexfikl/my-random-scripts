# SPDX-FileCopyrightText: 2021 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from __future__ import annotations

import math
import struct

import mpmath


def iter_float32() -> float:
    for i in range(2**32):
        b = struct.pack(">I", i)
        yield struct.unpack(">f", b)[0]


def float_to_ordered_int(x: float) -> int:
    # Pack the float into 8 bytes (big-endian) and then unpack as a signed 64-bit int.
    i = struct.unpack("!q", struct.pack("!d", x))[0]

    # For negative numbers, adjust so that the ordering is monotonic.
    return (0x8000000000000000 - i) if i < 0 else i


def ulp_diff(a: float, b: float) -> int:
    return abs(float_to_ordered_int(a) - float_to_ordered_int(b))


def sin_ulp_accuracy(x: float, high_prec_bits: int = 200) -> tuple[int, float, float]:
    mpmath.mp.prec = high_prec_bits

    high_prec_sin = float(mpmath.sin(x))
    libm_sin = math.sin(x)

    # Calculate the ULP difference.
    diff = ulp_diff(high_prec_sin, libm_sin)

    return diff, high_prec_sin, libm_sin


# Example test for a single value:
for x in iter_float32():
    diff, ref, lib_val = sin_ulp_accuracy(x)
    if diff > 0:
        print(f"Testing sin({x}):")
        print("High-precision reference (rounded):", ref)
        print("math.sin:", lib_val)
        print("ULP difference:", diff)
