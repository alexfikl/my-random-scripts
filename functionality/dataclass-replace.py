# SPDX-FileCopyrightText: 2021 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
Check if dataclass fields can be replaced when ``init=False``.
"""

from __future__ import annotations

from dataclasses import dataclass, field, replace


@dataclass
class MyClass:
    a: bool
    b: float = field(init=False, repr=False)

    def __post_init__(self):
        self.b = 2.0 * self.a - 1.0


if __name__ == "__main__":
    x = MyClass(a=True)
    print(x)

    # NOTE: this should throw a ValueError
    replace(x, b=10)
    print(x)
