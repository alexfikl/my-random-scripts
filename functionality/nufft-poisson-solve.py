# SPDX-FileCopyrightText: 2021 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
Ported from
https://finufft.readthedocs.io/en/latest/tutorial/peripois2d.html
"""

from __future__ import annotations

import pathlib

import matplotlib.pyplot as mp
import numpy as np

# {{{ one-dimensional Poisson solver


def source1d(x: np.ndarray, *, w0: float = 0.1) -> np.ndarray:
    return np.exp(-0.5 * (x - 1) ** 2 / w0**2)


def finufft1d_poisson(
    filename: str, n: int, *, order: int = 3, atol: float = 1.0e-12
) -> np.ndarray:
    """Solves a one-dimensional Poisson problem on a composite grid of
    Gauss-Legendre quadrature points.

    :param filename: solution plot.
    :param n: number of elements.
    :param order: order of the quadrature in each element.
    """

    # {{{ setup

    # get Gauss-Legendre quadrature on reference element
    from numpy.polynomial.legendre import leggauss

    xi, w = leggauss(order)

    # translate to each element
    v = np.linspace(0.0, 2.0 * np.pi, n + 1)
    xm = (v[1:] + v[:-1]).reshape(-1, 1) / 2.0
    dx = (v[1:] - v[:-1]).reshape(-1, 1) / 2.0
    x = (xm + dx * xi).ravel()
    w = (dx * w).ravel()

    # evaluate source terms
    f = source1d(x)
    fw = f * w / (2.0 * np.pi)

    # }}}

    # {{{ solve

    nk = int(2 * np.ceil(n / 4))
    k = np.fft.fftfreq(nk, d=1.0 / nk)

    # inverse Laplacian in Fourier space
    kfilter = 1.0 / k**2
    # remove average (to fix constant in the problem)
    kfilter[0] = 0.0
    # remove high frequency modes from quadrature
    kfilter[nk // 2] = 0.0

    import finufft

    fhat = finufft.nufft1d1(x, fw, isign=1, eps=atol, n_modes=(nk,), modeord=1)

    u = finufft.nufft1d2(x, kfilter * fhat, isign=-1, eps=atol, modeord=1)
    u = np.real(u).ravel()

    avg = np.sum(u * dx * w)
    print(f"n = {n:<4d} nk = {nk:<4d} u[0] = {u[0]:.16e} {avg:.16e}")

    # }}}

    # {{{ plot

    fig, ax = mp.subplots(figsize=(10, 10))

    ax.plot(x, u)
    ax.plot(x, u, "k.")
    ax.plot(x, f, "k--")
    ax.set_xlabel("$x$")

    fig.savefig(filename)

    # }}}


# }}}


# {{{ two-dimensional Poisson


def warp2d(x: np.ndarray, y: np.ndarray) -> np.ndarray:
    return np.stack([
        x + 0.5 * np.sin(x) + 0.2 * np.sin(2 * y),
        y + 0.3 * np.sin(2 * y) + 0.3 * np.sin(y - x),
    ])


def warp2d_jacobian(x: np.ndarray, y: np.ndarray) -> np.ndarray:
    return np.stack([
        [1 + 0.5 * np.cos(x), 0.4 * np.cos(2 * y)],
        [-0.3 * np.cos(y - x), 1 + 0.6 * np.cos(2 * y) + 0.3 * np.cos(y - x)],
    ])


def source2d(x: np.ndarray, y: np.ndarray, w0: float = 0.1) -> np.ndarray:
    # fmt: off
    result = (
        np.exp(-0.5 * ((x - 1) ** 2 + (y - 2) ** 2) / w0**2)
        - np.exp(-0.5 * ((x - 3) ** 2 + (y - 5) ** 2) / w0**2))
    # fmt: on

    return result


def finufft2d_poisson(filename: str, n: int, *, atol: float = 1.0e-12) -> np.ndarray:
    """Solves a two-dimensional Poisson problem on a warped grid.

    :param filename: solution plot.
    :param n: number of elements.
    :param order: order of the quadrature in each element.
    """

    # {{{ setup

    # uniform grid
    t = np.linspace(0.0, 2.0 * np.pi, n, endpoint=False)
    t = np.stack(np.meshgrid(t, t))
    # non uniform grid
    x = warp2d(t[0], t[1])

    # jacobian
    dx = warp2d_jacobian(t[0], t[1])
    detx = dx[0, 0] * dx[1, 1] - dx[0, 1] * dx[1, 0]

    # source term
    fw = source2d(x[0], x[1]) * detx / n**2

    # }}}

    # {{{ solve

    nk = int(2 * np.ceil(n / 4))
    k = np.fft.fftfreq(nk, d=1.0 / nk)
    k = np.stack(np.meshgrid(k, k))

    # inverse Laplacian in Fourier space
    kfilter = 1.0 / (k[0] ** 2 + k[1] ** 2)
    # remove average (to fix constant in the problem)
    kfilter[0, 0] = 0.0
    # remove high frequency modes from quadrature
    kfilter[nk // 2, :] = 0.0
    kfilter[:, nk // 2] = 0.0

    import finufft

    fhat = finufft.nufft2d1(
        x[0].ravel(),
        x[1].ravel(),
        fw.ravel().astype(np.complex128),
        isign=1,
        eps=atol,
        n_modes=(nk, nk),
        modeord=1,
    )

    u = finufft.nufft2d2(
        x[0].ravel(), x[1].ravel(), kfilter * fhat, isign=-1, eps=atol, modeord=1
    )
    u = np.real(u).reshape(n, n)

    avg = np.sum(u * detx / n**2)
    print(f"n = {n:<4d} nk = {nk:<4d} u[0, 0] = {u[0, 0]:.16e} {avg:.16e}")

    # }}}

    # {{{ plot

    fig, ax = mp.subplots(figsize=(12, 10))

    c = ax.contourf(x[0], x[1], u, levels=48)
    ax.set_xlabel("$x$")
    ax.set_ylabel("$y$")
    c = fig.colorbar(c, ax=ax)

    fig.savefig(filename, bbox_extra_artists=(c,))

    # }}}

    return u


# }}}


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("-n", type=int, default=80)
    parser.add_argument("--dim", type=int, default=2)
    parser.add_argument("--output", default=pathlib.Path(__file__).stem)
    args = parser.parse_args()

    if args.dim == 1:
        finufft1d_poisson(args.output, args.n)
    else:
        finufft2d_poisson(args.output, args.n)
