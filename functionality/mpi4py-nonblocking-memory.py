# SPDX-FileCopyrightText: 2021 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from __future__ import annotations

import numpy as np
from mpi4py import MPI


def send_buffer(comm, n: int, rank: int, *, tag: int):
    # NOTE: the data will go out of scope here and will be freed
    # by the gc, so this should crash somehow if mpi4py does not hold on to it
    return comm.isend(np.full(n, rank, dtype=np.float64), dest=1, tag=11)


def main():
    comm = MPI.COMM_WORLD
    size = comm.Get_size()
    rank = comm.Get_rank()
    assert size == 2

    if rank == 0:
        req = send_buffer(comm, 42, 42, tag=11)

        # NOTE: force a gc collect to make sure that the buffer in send_buffer
        # is gone for good
        import gc

        gc.collect()

        req.wait()
    else:
        req = comm.irecv(source=0, tag=11)
        x = req.wait()
        print(x)


if __name__ == "__main__":
    main()
