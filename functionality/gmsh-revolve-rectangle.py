# SPDX-FileCopyrightText: 2021 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
Slightly non-trivial example using :mod:`gmsh` Python bindings.
"""

from __future__ import annotations

import sys
from contextlib import contextmanager

import gmsh


@contextmanager
def geometry(name):
    gmsh.initialize()
    gmsh.model.add(name)

    gmsh.option.setNumber("Geometry.LineNumbers", 1)
    gmsh.option.setNumber("Geometry.SurfaceNumbers", 1)
    gmsh.option.setNumber("General.GraphicsFontSize", 32)
    gmsh.option.setNumber("General.MouseHoverMeshes", 1)

    try:
        yield
    finally:
        gmsh.clear()
        gmsh.finalize()


def make_rectangle(coordinates, mesh_size=1):
    npoints = len(coordinates)

    # make some points for rectangle corners
    points = [gmsh.model.geo.addPoint(*p, mesh_size) for p in coordinates]
    points += [points[0]]

    # stitch points in order into lines
    lines = [gmsh.model.geo.addLine(points[i], points[i + 1]) for i in range(npoints)]

    rect = gmsh.model.geo.addCurveLoop(lines)
    return rect, lines


def rotate(tags, origin=None, axis=None, angle=None):
    return gmsh.model.geo.rotate(tags, *origin, *axis, angle)


def revolve(tags, origin=None, axis=None, angle=None):
    return gmsh.model.geo.revolve(
        tags, *origin, *axis, angle, numElements=[], heights=[], recombine=False
    )


with geometry("Rectangle"):
    # make a rectangle
    points = [(0, 0, 0), (0, 10, 0), (10, 10, 0), (10, 0, 0)]
    rect, lines = make_rectangle(points, mesh_size=0.5)

    # stitch it into a surface
    surface = gmsh.model.geo.addPlaneSurface([rect])

    # revolve it to create a volume
    rotate([(2, surface)], origin=(0, 0, 0), axis=(1, 0, 0), angle=-0.0436)
    tags = revolve([(2, surface)], origin=(0, 0, 0), axis=(1, 0, 0), angle=0.174)
    print(tags)

    # name boundaries
    boundary_tags = [
        (2, surface, "back"),
        (*tags[0], "front"),
        # NOTE: these are exactly in the same order as `lines`
        (*tags[2], "left"),  # (p1, p2)
        (*tags[3], "top"),  # (p2, p3)
        (*tags[4], "right"),  # (p3, p4)
        # NOTE: adding the axis for fun
        (1, lines[-1], "axis"),
    ]
    print(boundary_tags)

    for dim, tag, name in boundary_tags:
        p = gmsh.model.addPhysicalGroup(dim, [tag])
        gmsh.model.setPhysicalName(dim, p, name)

    gmsh.model.geo.synchronize()
    gmsh.model.mesh.generate(3)
    gmsh.write("revolved-rectangle.msh")

    if "-nopopup" not in sys.argv:
        gmsh.fltk.run()
