# SPDX-FileCopyrightText: 2021 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from __future__ import annotations

import h5py
import numpy as np


def main(n=42):
    # initialize mpi
    from mpi4py import MPI

    mpicomm = MPI.COMM_WORLD
    mpirank = mpicomm.Get_rank()
    mpisize = mpicomm.Get_size()
    print(f"Rank: {mpirank} / {mpisize}")

    # get a chunk of data
    local_data = np.full((3, n), mpirank)

    # gather process slices
    total_size = mpicomm.allreduce(n)
    endpoint = mpicomm.scan(n)

    local_slice = np.s_[endpoint - n : endpoint]
    print(f"slice {local_slice} total {total_size}")

    with h5py.File("example.h5", "w", driver="mpio", comm=mpicomm) as h5:
        grp = h5.create_group("MyGroup")
        dset = grp.create_dataset(
            "MyDataset",
            shape=(3, total_size),
            dtype=local_data.dtype,
        )
        dset[:, local_slice] = local_data


if __name__ == "__main__":
    main()
