# SPDX-FileCopyrightText: 2021 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

r"""
Look at the FFT of various associated Legendre functions.

Notes:

- taking odd :math:`m` in :math:`P^m_n` will result in a square root factor.
  When evaluating at a :math:`\cos \theta`, that becomes a sine, so the modes
  are not trivial.
"""

from __future__ import annotations

import numpy as np


def run_fft_legendre(npoints: int, *, m: int = 3, n: int = 5) -> None:
    d = np.pi / npoints
    theta_d = np.linspace(d / 2.0, np.pi - d / 2.0, npoints)
    theta_0 = np.linspace(0.0, np.pi, npoints, endpoint=False)

    # {{{ evaluate legendre function

    from scipy.special import lpmv

    pmn_d = lpmv(m, n, np.cos(theta_d))
    pmn_0 = lpmv(m, n, np.cos(theta_0))

    from libscratch import figure

    with figure(f"fft-legendre-{m}-{n}-pmn") as fig:
        ax = fig.gca()
        ax.plot(theta_d, pmn_d)
        ax.set_xlabel(r"$\theta$")
        ax.set_ylabel(r"$P^m_n(\cos \theta)$")

    # }}}

    # {{{ evaluate FFT of legendre function

    k = np.fft.fftfreq(npoints, d=1.0 / npoints)
    i = np.argsort(k)
    k = k[i]

    pk_d = np.fft.fft(pmn_d)[i]
    pk_0 = np.fft.fft(pmn_0)[i]

    with figure(f"fft-legendre-{m}-{n}-modes") as fig:
        ax = fig.gca()
        ax.plot(k, pk_0.real, "o-", label=r"$\Re$")
        ax.plot(k, pk_0.imag, "o--", label=r"$\Im$")
        ax.plot(k, pk_d.real, "k-", label=r"$\Re$")
        ax.plot(k, pk_d.imag, "k--", label=r"$\Im$")

        ax.set_xlabel("$k$")
        ax.set_ylabel(r"$\hat{P}^m_n$")

    # }}}


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("-m", type=int, default=2)
    parser.add_argument("-n", type=int, default=5)
    parser.add_argument("--npoints", type=int, default=64)

    args = parser.parse_args()
    run_fft_legendre(args.npoints, m=args.m, n=args.n)
