# SPDX-FileCopyrightText: 2021 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from __future__ import annotations

import numpy as np
import sympy as sp


def find_corner_curve(*, p0: float, p1: float, d1: float, order: int) -> sp.Expr:
    r"""Fit a curve :math:`p(x)` such that

    * :math:`p(-1) = p0`
    * :math:`p(+1) = p1`
    * :math:`p'(+1) = d1`
    * all higher order derivatives up to *order* are zero at :math:`\pm 1`.
    """
    assert order >= 1

    x = sp.Symbol("x", real=True)

    c = tuple([sp.Symbol(f"c{i}") for i in range(2 * (order + 1))])
    p = sum(c_i * sp.legendre(i, x) for i, c_i in enumerate(c))
    print(p)

    constraints = [p.subs(x, -1) - p0, p.subs(x, +1) - p1]
    constraints += [p.diff(x).subs(x, +1) - d1]
    constraints += [p.diff(x, i).subs(x, -1) for i in range(1, order + 1)]
    constraints += [p.diff(x, i).subs(x, +1) for i in range(2, order + 1)]
    rx = sp.solve(constraints, *c)
    print(rx)
    print(p.subs(rx))

    return p.subs(rx)


def plot_corner_curve(p: sp.Expr, *, p0: float, p1: float, d1: float) -> None:
    x = sp.Symbol("x", real=True)
    func = sp.lambdify(x, p, modules="numpy")
    dfunc = sp.lambdify(x, p.diff(x), modules="numpy")

    x = np.linspace(-1, 1, 512)

    import matplotlib.pyplot as mp

    fig = mp.figure(figsize=(10, 10), dpi=300)

    # {{{ plot curve

    ax = fig.gca()
    ax.grid(both=True)

    ax.plot([-2, 0], [p0, p0], "k--")
    ax.plot([0, +2], [p0, p0 + 2 * d1], "k--")
    ax.plot(x, func(x), lw=5)
    ax.set_xlabel("$x$")
    ax.set_ylabel("$p(x)$")

    fig.savefig("corner-order-fitting-curve")
    fig.clf()

    # }}}

    # {{{ plot first derivative

    ax = fig.gca()
    ax.grid(both=True)

    ax.plot(x, dfunc(x))
    ax.set_xlabel("$x$")
    ax.set_ylabel("$p'(x)$")

    fig.savefig("corner-order-fitting-derivative")
    fig.clf()

    # }}}


if __name__ == "__main__":
    r = find_corner_curve(p0=0.0, p1=1.0, d1=1.0, order=6)
    plot_corner_curve(r, p0=0.0, p1=1.0, d1=1.0)
