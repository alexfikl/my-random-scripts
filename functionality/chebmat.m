% SPDX-FileCopyrightText: 2021 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [z,D1,D2,IntW] = chebmat(N,zmax,mapping,zp)
%
% [z,D1,D2,W] = chebmat(N,zmax,mapping,zp)
%
% This routine sets up the first- and second spectral derivatives
% discretization matrices and the weight matrix for spectral integration.
%
% Input:
%   N: number of Chebychev nodes
%   zmax: wall-normal coordinate of the outer boundary of domain
%   mapping: mapping scheme between physical and spectral space.
%            can have values 'linear' or 'nonlinear'
%   zp: half of the points are located in [0 zp]
%
% output:
%   z: array containing wall-normal coordinates of nodes in physical space
%   D1, D2:  Matrices corresponding to first- and second wall-normal
%            derivatives
%   IntW:    weith of spectral integration
%
% (c) Ardeshir Hanifi & David Tempelmann, 2014
%
%------------------------------------------------------------------

[x, D] = chebdif(N, 2);
D1 = D(:,:,1);
D2 = D(:,:,2);

% map to z = [0, zmax]
if strcmp(mapping,'linear')
    % map onto interval [0 ymax]
    D1 = D1*(2/zmax);
    D2 = D2*(2/zmax)^2;
    z=(x+1)*zmax/2;
    transfac = zmax/2;

elseif strcmp(mapping,'nonlinear')
    ac = zp*zmax/(zmax-2*zp);
    bc = 1+2*ac/zmax;
    z = ac*(1+x)./(bc-x);
    transfac = ac*(bc+1)./(bc-x).^2;
    D1 = diag(1./transfac)*D1;
    D2 = D1*D1;
else
    error('chebmat:mapping', 'Unknown mapping')
end

%get integration weights
Q = diag(transfac)*iwt(N);
IntW = blkdiag(Q,Q,Q,Q,Q);

end
